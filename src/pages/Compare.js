import React, { Component } from 'react'
import { connect } from 'react-redux'
import Navbar from './Components/Navbar'
import Footer from './Components/Footer'
import Select from 'react-select'
import isEqual from 'lodash/isEqual'

import '../Product.css'
import ProductActions from '../reducer/ProductRedux'
import CategoryActions from '../reducer/CategoryRedux'

// const productOption = [
//     { value: 101010, label: 'Samsung Galaxy S21 Ultra 5G' },
//     { value: 101011, label: 'Samsung Galaxy S20 FE' },
//     { value: 101013, label: 'Samsung Galaxy F41' },
//     { value: 101014, label: 'Samsung Galaxy Z Fold2 5G' },
//     { value: 101015, label: 'Samsung Galaxy Z Flip 5G' }
//   ]

const categoryOption = [
  { value: 'Handphone', label: 'Handphone' },
  { value: 'Laptop', label: 'Laptop' }
]

  const customStyles = {
    control: (styles) => ({
      ...styles,
      width: 400
    })
  }

class Compare extends Component {
  constructor () {
    super()
    this.state = {
      selectedProductOption1: null,
      selectedProductOption2: null,
      selectedProductOption3: null,
      productDetail1: null,
      productDetail2: null,
      productDetail3: null,
      selectedOption: 1,
      compareTotal: 1,
      itemList: [],
      insertProductOption: false,
      productOption: []
    }
  }

  componentDidMount () {
    // this.props.allProductRequest()
    this.props.clearProduct()
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.productDetail) {
      returnObj = {
        ...returnObj,
        productDetail1: props.productDetail
      }
    }
    if (props.product2Detail) {
      returnObj = {
        ...returnObj,
        productDetail2: props.product2Detail
      }
    }
    if (props.product3Detail) {
      returnObj = {
        ...returnObj,
        productDetail3: props.product3Detail
      }
    }
    if (props.allProductData) {
      let bool = true
      if(isEqual(props.allProductData[0], state.itemList)) {
        bool = true
      } else {
        bool = false
      }
      returnObj = {
        ...returnObj,
        itemList: props.allProductData[0],
        insertProductOption: bool
      }
    }
    return returnObj
  }

  componentDidUpdate() {
    const { itemList, insertProductOption } = this.state
    if(itemList.length !== 0 && insertProductOption === false) {
      let arrayItem = []
      itemList.map(item => {
        const arrValue = {
          value: item.ProductID,
          label: item.Title
        }
        return (
          arrayItem.push(arrValue)
        )
      })
      this.setState({insertProductOption: true, productOption: arrayItem})
    }
  }
    
  handleProductChange = (selectedOption, i) => {
    if(i === 1) {
      this.setState({ selectedProductOption1: selectedOption });
      this.props.productRequest(selectedOption.value)
    } else if(i === 2) {
      this.setState({ selectedProductOption2: selectedOption });
      this.props.product2Request(selectedOption.value)
    } else if(i === 3) {
      this.setState({ selectedProductOption3: selectedOption });
      this.props.product3Request(selectedOption.value)
    }
  };

  addProductCompare = () => {
    this.setState({ compareTotal : this.state.compareTotal + 1})
  }

  componentWillUnmount () {
    this.props.clearProduct()
  }

  renderProductSpec = () => {
    const { productDetail1 } = this.state
    const spec  = productDetail1.Spesification
    // console.log('htmlres ', spec)
    if(productDetail1) {
      if(productDetail1.Category === 'Handphone') {
        return <td dangerouslySetInnerHTML={{__html: spec}} />
      } else {
        return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
      }
    } else {
      return null
    }
    
    // return <div dangerouslySetInnerHTML={{__html: spec}} />
  }

  renderProductSpec2 = () => {
    const { productDetail2 } = this.state
    // console.log('productDetail2', productDetail2)
    const spec  = productDetail2.Spesification
    if(productDetail2.Category === 'Handphone') {
      return <td dangerouslySetInnerHTML={{__html: spec}} />
    } else {
      return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
    }
    // return <td dangerouslySetInnerHTML={{__html: spec}} />
  }

  renderProductSpec3 = () => {
    const { productDetail3 } = this.state
    // console.log('productDetail3', productDetail3)
    const spec  = productDetail3.Spesification
    if(productDetail3) {
      if(productDetail3.Category === 'Handphone') {
        return <td dangerouslySetInnerHTML={{__html: spec}} />
      } else {
        return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
      }
    } else {
      return null
    }
    // return <td dangerouslySetInnerHTML={{__html: spec}} />
  }

  handleCategoryChange = (selectedCategory) => {
    this.props.clearProduct()
    this.setState({
      selectedCategory: selectedCategory, 
      insertProductOption: false, 
      itemList: [], 
      productOption: [], 
      productDetail1: null, 
      productDetail2: null, 
      productDetail3: null,
      selectedProductOption1: null,
      selectedProductOption2: null,
      selectedProductOption3: null,
      compareTotal: 1
    })
    this.props.categoryProductRequest(selectedCategory.value)
  }

  render () {
      const { selectedCategory, selectedProductOption1, selectedProductOption2, selectedProductOption3, productDetail1, productDetail2, productDetail3, compareTotal, productOption } = this.state
    return (
      <div>
        <Navbar />
        <div className='compare-container margin-top-xl'>
          <div className='bold'>
            COMPARE PRODUCT
          </div>
        <div>
            <div className='margin-top-s'> Choose Category</div>
            <div className='margin-top-xxs' style={{width: '400px'}}><Select
                  value={selectedCategory}
                  onChange={(selectedCategory) => this.handleCategoryChange(selectedCategory)}
                  options={categoryOption}
                  placeholder='Choose a category...'
                /></div>
        </div>
          <div className='margin-top-s'>
            Choose Product
          </div>
          <div className='flex'>
            <div className='product-compare-container'>
              <Select
                styles={customStyles}
                value={selectedProductOption1}
                onChange={(selectedProductOption1) => this.handleProductChange(selectedProductOption1, 1)}
                options={productOption}
                placeholder='Pilih Produk...'
              />
            </div>
            {
              compareTotal > 1
              ? <div className='product-compare-container margin-left-s'>
                <Select
                  styles={customStyles}
                  value={selectedProductOption2}
                  onChange={(selectedProductOption2) => this.handleProductChange(selectedProductOption2, 2)}
                  options={productOption}
                  placeholder='Pilih Produk...'
                  />
                </div>
              : null
            }
            {
              compareTotal > 2
              ? <div className='product-compare-container margin-left-s'>
                <Select
                  styles={customStyles}
                  value={selectedProductOption3}
                  onChange={(selectedProductOption3) => this.handleProductChange(selectedProductOption3, 3)}
                  options={productOption}
                  placeholder='Pilih Produk...'
                  />
                </div>
              : null
            }
            {
              compareTotal < 3
              ? <div className='add-compare-container cursor-pointer' onClick={() => this.addProductCompare()}>
                  {/* <div style={{border: '1px solid gray', 'borderRadius': '15px', 'padding': '4px 7px 4px 7px'}}> */}
                  <div className='compare-add-button'>
                    + add more...
                  </div>
                </div>
              : null
            }
          </div>
          <div className='flex'>
          {(productDetail1)
                ? <div className='compare-product-container margin-bottom-xs'>
                  <table className='spec'>
                  {/* {productDetail1
                    ? Object.keys(productDetail1).map((details, index) => {
                        if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto' && details !== 'HighestPrice' && details !== 'LowestPrice' && details !== 'Category') {
                          return (
                            <Fragment key={details}>
                              <thead>
                                <tr>
                                  <th>
                                    <div className='margin-top-s bold float-left margin-bottom-xxxs'>
                                      {details}
                                    </div>
                                  </th>
                                </tr>
                              </thead>
                              <tbody key={details}>
                                {typeof (productDetail1[details]) === 'object'
                                  ? Object.keys(productDetail1[details]).map((child, index) =>
                                    <tr key={child}>
                                      <td className='spec'>
                                        {child}
                                      </td>
                                      <td className='spec'>
                                        {productDetail1[details][child]}
                                      </td>
                                    </tr>
                                    )
                                  : null}
                              </tbody>
                            </Fragment>
                          )
                        } else {
                          return null
                        }
                      }
                    
                      )
                    : null} */}
                    {productDetail1
                      ? this.renderProductSpec()
                      : null
                    }
                  </table>
                  </div>
                  : null
              }
            {(productDetail2)
                ? <div className='compare-product-container  margin-left-s'>
                  <table className='spec'>
                  {productDetail2
                    ? this.renderProductSpec2()
                    : null
                    // ? Object.keys(productDetail2).map((details, index) => {
                    //     if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto' && details !== 'HighestPrice' && details !== 'LowestPrice' && details !== 'Category') {
                    //       return (
                    //         <Fragment key={details}>
                    //           <thead>
                    //             <tr>
                    //               <th>
                    //                 <div className='margin-top-s bold float-left margin-bottom-xxxs'>
                    //                   {details}
                    //                 </div>
                    //               </th>
                    //             </tr>
                    //           </thead>
                    //           <tbody key={details}>
                    //             {typeof (productDetail2[details]) === 'object'
                    //               ? Object.keys(productDetail2[details]).map((child, index) =>
                    //                 <tr key={child}>
                    //                   <td className='spec'>
                    //                     {child}
                    //                   </td>
                    //                   <td className='spec'>
                    //                     {productDetail2[details][child]}
                    //                   </td>
                    //                 </tr>
                    //                 )
                    //               : null}
                    //           </tbody>
                    //         </Fragment>
                    //       )
                    //     } else {
                    //       return null
                    //     }
                    //   }
                    
                    //   )
                    // : null
                  }
                  </table>
                  </div>
                  : null
              }
            {(productDetail3)
                ? <div className='compare-product-container  margin-left-s'>
                  <table className='spec'>
                  {productDetail3
                    ?this.renderProductSpec3()
                    : null
                    // ? Object.keys(productDetail3).map((details, index) => {
                    //     if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto' && details !== 'HighestPrice' && details !== 'LowestPrice' && details !== 'Category') {
                    //       return (
                    //         <Fragment key={details}>
                    //           <thead>
                    //             <tr>
                    //               <th>
                    //                 <div className='margin-top-s bold float-left margin-bottom-xxxs'>
                    //                   {details}
                    //                 </div>
                    //               </th>
                    //             </tr>
                    //           </thead>
                    //           <tbody key={details}>
                    //             {typeof (productDetail3[details]) === 'object'
                    //               ? Object.keys(productDetail3[details]).map((child, index) =>
                    //                 <tr key={child}>
                    //                   <td className='spec'>
                    //                     {child}
                    //                   </td>
                    //                   <td className='spec'>
                    //                     {productDetail3[details][child]}
                    //                   </td>
                    //                 </tr>
                    //                 )
                    //               : null}
                    //           </tbody>
                    //         </Fragment>
                    //       )
                    //     } else {
                    //       return null
                    //     }
                    //   }
                    
                    //   )
                    // : null
                    }
                  </table>
                  </div>
                  : null
              }
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  allProductData: state.category.categoryProduct,
  productDetail: state.product.productDetail,
  product2Detail: state.product.product2Detail,
  product3Detail: state.product.product3Detail
})

const mapDispatchToProps = dispatch => {
  return {
    productRequest: (payload) => dispatch(ProductActions.productRequest(payload)),
    product2Request: (payload) => dispatch(ProductActions.product2Request(payload)),
    product3Request: (payload) => dispatch(ProductActions.product3Request(payload)),
    clearProduct: () => dispatch(ProductActions.clearProduct()),
    categoryProductRequest: (payload) => dispatch(CategoryActions.categoryProductRequest(payload)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Compare)

