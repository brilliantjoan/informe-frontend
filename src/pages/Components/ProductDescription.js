import React, { Component } from 'react'

class ProductDescription extends Component {
  constructor () {
    super()
    this.state = {
      productDetail: null
    }
  }

  static getDerivedStateFromProps (props, state) {
    if (props.productDetail) {
      return {
        productDetail: props.productDetail
      }
    }

    return null
  }

  renderProductSpec = () => {
    const { productDetail } = this.state
    const spec  = productDetail.Spesification
    if(productDetail.category === 'Handphone') {
      return <td dangerouslySetInnerHTML={{__html: spec}} />
    } else {
      return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
    }
  }

  render () {
    const { productDetail } = this.state
    return (
      <div>
        <table className='spec'>
          {/* {productDetail
            ? Object.keys(productDetail).map((details, index) => {
                if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto') {
                  return (
                    <Fragment key={details}>
                      <thead>
                        <tr>
                          {details !== 'LowestPrice' && details !== 'HightestPrice' && details !== 'Category'
                            ? <th>
                              <div className='margin-top-xs bold float-left margin-bottom-xxxs'>
                                {details}
                              </div>
                            </th>
                            : null}
                        </tr>
                      </thead>
                      <tbody key={details}>
                        {typeof (productDetail[details]) === 'object'
                          ? Object.keys(productDetail[details]).map((child, index) =>
                            <tr key={child}>
                              <td className='spec'>
                                {child}
                              </td>
                              <td className='spec'>
                                {productDetail[details][child]}
                              </td>
                            </tr>
                            )
                          : null}
                      </tbody>
                    </Fragment>
                  )
                } else {
                  return null
                }
              }

              )
            : null} */}
        { productDetail
          ? this.renderProductSpec()
          : null
        }
        </table>
      </div>
    )
  }
}

export default ProductDescription
