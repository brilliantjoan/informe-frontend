import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import localforage from 'localforage'
import ReactPaginate from 'react-paginate'

import CommentActions from '../../reducer/CommentRedux'

import user from '../../images/profile.png'

class Discussion extends Component {
  constructor () {
    super()
    this.state = {
      productId: null,
      currentComment: '',
      comments: null,
      offset: 0,
      perPage: 5,
      currentPage: 0,
      elements: [],
      countData: null,
      role: '',
      isComment: null,
      currentReplyComment: '',
      initialFetch: true,
      fetchComment: true
    }
  }

  async componentDidMount() {
    const role = await localforage.getItem('role')
    this.setState({role: role})
    // const commentPayload = {
    //   productId : 12012,
    //   limit: 100,
    //   offset: 0
    // }
    // this.props.commentRequest(commentPayload)
  }

  static getDerivedStateFromProps(props, state) {
    let returnObj = {}
    if(props.comment) {
      returnObj = {
        ...returnObj,
        comments: props.comment.comment,
        countData: props.comment.count_data
      }
    }

    if(props.productId && state.initialFetch === true) {
      const commentPayload = {
        productId : props.productId,
        limit: 100,
        offset: 0
      }
      props.commentRequest(commentPayload)

      returnObj =  {
        ...returnObj,
        initialFetch: false,
        productId: props.productId
      }
    }
    return returnObj
  }

  componentDidUpdate() {
    const { comments, fetchComment } = this.state
    
    if (comments && fetchComment === true) {
      this.setCurrentPage()
      this.setState({fetchComment: false})
    }
  }

  handleCommentChange = (e) => {
    this.setState({
      currentComment: e.target.value
    })
  }
  
  handleReplyCommentChange = (e) => {
    this.setState({
      currentReplyComment: e.target.value
    })
  }

  handleSendComment = async () => {
    const { currentComment, productId } = this.state
    const name = await localforage.getItem('name')
    const userId = await localforage.getItem('cust_id')
    if (name && userId) {
      const commentPayload = {
        userId: parseInt(userId),
        // name: email.split('@')[0],
        // name: name,
        productId : parseInt(productId),
        comment: currentComment
      }
      this.props.insertCommentRequest(commentPayload)
      window.alert("message posted")
      window.location.reload()
    } else {
      this.props.history.push('/login')
    }
  }

  handleSendReply = async (discussionId) => {
    const { currentReplyComment } = this.state
    // console.log('discussion id', discussionId)
    const token = await localforage.getItem('token')
    const userId = await localforage.getItem('cust_id')
    if (token && userId) {
      const replyPayload = {
        user_id: parseInt(userId),
        discussion_id : discussionId,
        reply_comment: currentReplyComment
      }
      this.props.insertReplyRequest(replyPayload)
      window.alert("reply posted")
      window.location.reload()
    } else {
      this.props.history.push('/login')
    }
  }

  handlePageClick = (data) => {
    const selectedPage = data.selected
    const offset = selectedPage * this.state.perPage
    this.setState({
      currentPage: selectedPage, offset: offset
    }, () => {
      this.setCurrentPage()
    })
  }

  setCurrentPage = () => {
    const { comments, offset, perPage } = this.state
    let elements = comments.slice(offset, offset + perPage)
    this.setState({
      elements: elements
    })
  }

  replyClick = (index) => {
    this.setState({isComment: index})
  }

  deleteComment = (discussionId, replyDiscussionId) => {
    // const { productId } = this.state  
    const deletePayload = {
      discussion_id : discussionId,
      reply_discussion_id: replyDiscussionId
    }
    if (window.confirm("Are you sure you want to delete this comment?")) {
      this.props.deleteCommentRequest(deletePayload)
      window.location.reload()
      // this.props.history.push({
      //   pathname: '/product',
      //   state: {
      //     productId: productId,
      //     tab: 'discussion'
      //   }
      // })
    }
  }

  render () {
    const { currentComment, comments, currentPage, elements, role, isComment, currentReplyComment } = this.state
    return (
      <div className='margin-top-xs'>
        Discussion ({comments ? comments.length : 0})
        <div className='flex margin-top-s'>
          <input type='text' value={currentComment} onChange={(e) => this.handleCommentChange(e)} className='commentbox' placeholder='Write comment'/>
          <div className='sendcomment' onClick={this.handleSendComment}>
            Send
          </div>
        </div>

        {elements
          ? elements.map((dis, index) => {
            return (
              <div key={index}>
                <div className="comment-container">
                  <div className='flex'>
                  <div className='comment-pic-container'>
                    {/* <div className='circle' /> */}
                    <img src={user} alt='no pic' height='65px' />
                  </div>
                   <div style={{margin: '40px 0 0 15px'}}>
                     {dis.name}
                   </div>
                  </div>
                  <div style={{margin: '3px 0 20px 110px'}}>
                    {dis.comment}
                  {role === 'admin'
                  ? <span className='deletecomment' onClick={() => this.deleteComment(dis.discussion_id, null)}>
                      Delete
                    </span>
                  : null
                  }
                  <span className='reply-button margin-right-xxs' onClick={() => this.replyClick(index)}>
                    reply
                  </span>
                    
                  </div>
                </div>

                {isComment === index
                  ? <div className='flex margin-top-s'>
                      <input type='text' value={currentReplyComment} onChange={(e) => this.handleReplyCommentChange(e)} className='commentbox' placeholder='Write comment here'/>
                      <div className='sendcomment' onClick={() => this.handleSendReply(dis.discussion_id)}>
                        Send
                      </div>
                    </div>
                  : null
                }
                {dis.replies
                  ? dis.replies.map((replies, index) => { 
                    return (
                      <div key={index}>
                        <div className="comment-reply-container">
                          <div className='flex'>
                          <div className='comment-pic-container'>
                            <img src={user} alt='no pic' height='65px' />
                          </div>
                           <div style={{margin: '40px 0 0 15px'}}>
                             {replies.name}
                           </div>
                          </div>

                          <div style={{margin: '3px 0 20px 110px'}}>
                          {replies.reply_comment}
                          {role === 'admin'
                            ? <span className='deletecomment' onClick={() => this.deleteComment(dis.discussion_id, replies.reply_discussion_id)}>
                                Delete
                              </span>
                            : null
                          }
                          </div>
                        </div>
                      </div> 
                    )  
                  })
                  : null
                }
              </div> 
            )
          }) 
          : null
        }

        {comments && comments.length > 0
          ? <ul>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={comments.length / 5}
            forcePage={currentPage}
            marginPagesDisplayed={1}
            pageRangeDisplayed={4}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages-pagination'}
            activeClassName={'active'}
          />
        </ul>

          : null
        }  
      </div>
    )
  }
}

const mapStateToProps = state => ({
  comment: state.comment.comment
})

const mapDispatchToProps = dispatch => {
  return {
    commentRequest: (payload) => dispatch(CommentActions.commentRequest(payload)),
    insertCommentRequest: (payload) => dispatch(CommentActions.insertCommentRequest(payload)),
    insertReplyRequest: (payload) => dispatch(CommentActions.insertReplyRequest(payload)),
    deleteCommentRequest: (payload) => dispatch(CommentActions.deleteCommentRequest(payload))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Discussion))
