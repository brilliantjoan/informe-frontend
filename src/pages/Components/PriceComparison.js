import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AiFillStar } from 'react-icons/ai'
import ReactPaginate from 'react-paginate'

import PriceActions from '../../reducer/PriceRedux'

import tokped from '../../images/tokped.png'
import blibli from '../../images/blibli.jpg'

class PriceComparison extends Component {
  constructor () {
    super()
    this.state = {
      list: null,
      filter: 'sold',
      offset: 0,
      perPage: 10,
      currentPage: 0,
      elements: []
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.price) {
      returnObj = {
        ...returnObj,
        list: props.price
      }
    }

    return returnObj
  }

  componentDidMount () {
    const { productId, productName } = this.props

    const pricePayload = {
      productId: productId,
      productName: productName
    }
    this.props.priceRequest(pricePayload)

  }

  componentDidUpdate() {
    const { list, elements } = this.state

    if(list && elements.length === 0) {
      this.setCurrentPage()
    }
  }
  
  handleFilterPrice = (filter) => {
    this.setState({
      filter: filter,
      offset: 0,
      currentPage: 0
    }, () => {
      this.setCurrentPage()
    })
  }

  handlePageClick = (data) => {
    const selectedPage = data.selected
    const offset = selectedPage * this.state.perPage
    this.setState({
      currentPage: selectedPage, offset: offset
    }, () => {
      this.setCurrentPage()
    })
  }

  setCurrentPage = () => {
    const { list, offset, perPage, filter } = this.state
    if(!filter) {
      let elements = list.slice(offset, offset + perPage)
      this.setState({
        elements: elements
      })
    } else {
      if(filter === 'sold') {
        let elements = list.sort((a,b) => {
          // let b1 = parseInt(b.SoldQty.substring(8))  
          // let a1 = parseInt(a.SoldQty.substring(8))
          let b1 = b.SoldQty  
          let a1 = a.SoldQty
          if(a1 === '' || a1 === null) return null
          if(b1 === '' || b1 === null) return null
          return b1 - a1
        }).slice(offset, offset + perPage)
        this.setState({
          elements: elements
        })
      } else if(filter === 'price') {
        let elements = list.sort((a,b) => parseInt(a.Price.split('.').join("").substring(2)) - parseInt(b.Price.split('.').join("").substring(2))).slice(offset, offset + perPage)
        this.setState({
          elements: elements
        })
      }
    }
  }

  render () {
    const { list, filter, currentPage, elements } = this.state
    return (
      <div className='margin-top-xs'>
        <div className='flex'>
          <div className='margin-top-xxxs color-grey'>
            Sort by
          </div>
          {/* <div className='' > */}
            <div className={(filter === 'sold') ? 'price-active-tab price-filter-button margin-left-xs' : 'margin-left-xs price-filter-button'} onClick={() => this.handleFilterPrice('sold')}>
              Sold
            </div>
          {/* </div> */}
          <div className={(filter === 'price') ? 'price-active-tab price-filter-button margin-left-xs' : 'margin-left-xs price-filter-button'} onClick={() => this.handleFilterPrice('price')}>
            Price
          </div>
        </div>

        <div className='flex flex-wrap margin-top-xxs'>
          {elements
            ? elements.map((l, index) => {
              if (l.Web === 'blibli') {
                l.UrlProduct = 'https://' + l.UrlProduct
              }
                return (
                  <div key={index} className='price-compare-margin flex margin-right-s'>
                    <div>
                      {l.Web === 'tokopedia'
                      ? <img src={tokped} alt='No Pic' className='price-image' />
                      :<img src={blibli} alt='No Pic' className='price-image' />
                      }
                    </div>

                    <div className='margin-left-xxs margin-right-xxs margin-top-xs margin-bottom-xxxs width-100'>
                      <div>
                        {l.Name}
                      </div>
                      <div className='margin-top-xs'>
                        Terjual {l.SoldQty}
                      </div>
                      <div className='margin-top-xs'>
                        <AiFillStar /> {(l.Rating)? l.Rating : '0'}
                        <a href={l.UrlProduct}>
                          <span className='price-store cursor-pointer' href={l.UrlProduct}>View Store</span>
                        </a>
                      </div>
                      <div className='margin-top-xs'>
                        {l.Price}
                      </div>
                    </div>
                  </div>
                )
              })
            : null}
        </div>
        {list
        ?  <ul>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={list.length / 10}
            forcePage={currentPage}
            marginPagesDisplayed={1}
            pageRangeDisplayed={4}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages-pagination'}
            activeClassName={'active'}
          />
          </ul>
      : null
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  price: state.price.priceData
})

const mapDispatchToProps = dispatch => {
  return {
    priceRequest: (payload) => dispatch(PriceActions.priceRequest(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PriceComparison)
