import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class ProductPeopleChoose extends Component {
  constructor () {
    super()
    this.state = {
      products: null
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.product) {
      returnObj = {
        ...returnObj,
        products: props.product
      }
    }

    return returnObj
  }

  render () {
    const { products } = this.state
    return (
      <div>
        Others also choose
        <div className='flex-container flex-wrap'>
          {products
            ? products.map((prod, index) => {
                let urlPhoto = ''
                let name = ''
                let productId = ''
                if (prod[0]) {
                  Object.keys(prod[0]).map((detail, index) => {
                    if (detail === 'Photo') {
                      urlPhoto = prod[0][detail]
                    }
                    if (detail === 'Title') {
                      name = prod[0][detail]
                    }
                    if (detail === 'ProductID') {
                      productId = prod[0][detail]
                    }
                    return null
                  })
                  if (urlPhoto && name) {
                    return (
                      <div className='product-border' key={name}>
                        <Link to={{
                          pathname: '/product',
                          state: {
                            productId: productId
                          }
                        }}
                        >
                          <div>
                            <img src={urlPhoto} alt='no pic' className='product-list-container' />
                          </div>
                        </Link>
                        <div className='product-text-container'>{name}</div>
                      </div>
                    )
                  } else { return null }
                }
              })

            : null}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  product: state.product.productPeopleChooseData
})

export default connect(mapStateToProps, null)(ProductPeopleChoose)
