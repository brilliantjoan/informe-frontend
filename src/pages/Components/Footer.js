import React, { Component } from 'react'

class Footer extends Component {
  render () {
    return (
      <div className='footer'>
        <div className='inner-footer'>
          © 2020 - Informe | All rights reserved
        </div>
      </div>
    )
  }
}

export default Footer
