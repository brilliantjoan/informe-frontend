import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
// import { FiSearch } from 'react-icons/fi'
import { connect } from 'react-redux'
import localforage from 'localforage'

import UserActions from '../../reducer/UserRedux'
import ProductActions from '../../reducer/ProductRedux'

class Navbar extends Component {
  constructor () {
    super()
    this.state = {
      token: null,
      showMenu: false,
      searchMessage: '',
      name: null
    }
  }

  async componentDidMount () {
    const token = await localforage.getItem('token')
    const name = await localforage.getItem('name')
    if (token) {
      this.setState({ 
        token: token 
      })
    }

    // if (email) {
    //   this.setState({
    //     email: email.split('@')[0] 
    //   })
    // }
    if (name) {
      this.setState({
        name: name
      })
    }
  }

  handleLogout = async () => {
    await localforage.removeItem('token')
    await localforage.removeItem('email')
    await localforage.removeItem('cust_id')
    await localforage.removeItem('role')
    await localforage.removeItem('name')
    this.props.logout()
    this.props.history.push('/login')
  }

  handleSearchChange = (e) => {
    this.setState({
      searchMessage: e.target.value
    })
  }

  searchProduct = (e) => {
    const { searchMessage } = this.state
    if(searchMessage && e.key === 'Enter') {
      this.props.searchRequest(searchMessage)
      this.props.history.push({
        pathname: '/search',
        state: {
          searchMessage: searchMessage
        }
      })
    }
  }

  showMenu = () => {
    this.setState ({ showMenu: true })
  }

  closeMenu = () => {
    this.setState ({ showMenu: false })
  }

  render () {
    const { token, name } = this.state
    return (
      <div className='background-white'>
        <div className='navbar nav-border'>
          <div>
            <div className='nav-comp'>
              <Link to='/'>
                <div style={{ fontWeight: 'bold' }} className='color-custom font-icon' href='#home'>InforMe</div>
              </Link>
            </div>

            <div className='nav-comp'>
              <Link to='/compare'>
                <div className='nav-text'>Compare</div>
              </Link>
            </div>

            <div className='nav-comp'>
              <Link to='/upgrade'>
                <div className='nav-text'>Upgrade Device</div>
              </Link>
            </div>

            <div className='nav-comp' style={{ width: '60%' }}>
                <input type='text' placeholder='Search for product' className='searchbar nav-text' onChange={(e) => this.handleSearchChange(e)} onKeyPress={this.searchProduct}/>
                {/* <FiSearch/> */}
            </div>

            {token
              ? <div className='float-right dropdown-menu'>
                  <div className='margin-right-xs'>
                     <div className='nav-comp float-right nav-text'>
                        <button onClick={this.showMenu} className='cursor-pointer'>
                          Hi, {name}
                        </button>
                      </div> 
                  </div>  
                 {/* {showMenu 
                  ?  */}
                  <div className='dropdown-children'>
                      <div className='float-right'>
                        <Link to='/profile'>
                          <div>Profile</div>
                        </Link>
                        <Link to='/wishlist'>
                          <div>Wishlist</div>
                        </Link>
                        <button onClick={this.handleLogout} className='cursor-pointer'>Logout</button>
                      </div>
                    </div> 
                  {/* // : null
                  // } */}
                </div>
              : <div className='nav-comp float-right margin-right-xs'>
                <Link to='/login'>
                  <div className='nav-text'>Login / Register</div>
                </Link>
              </div>}

          </div>
        </div>
      </div>
    )
  }
}

// import { FaBeer } from "@react-icons/all-files/fa/FaBeer";
// class Question extends React.Component {
//   render() {
//     return <h3> Lets go for a <FaBeer />? </h3>
//   }
// }

// import { IconContext } from "react-icons";

// <IconContext.Provider value={{ color: "blue", className: "global-class-name" }}>
//   <div>
//     <FaFolder />
//   </div>
// </IconContext.Provider>

const mapStateToProps = state => ({
  user: state.user.userData
})

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(UserActions.logout()),
    searchRequest: (payload) => dispatch(ProductActions.searchRequest(payload))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar))
