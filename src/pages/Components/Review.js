import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { AiFillStar } from 'react-icons/ai'
import localforage from 'localforage'
import ReactPaginate from 'react-paginate'
import isEmpty from 'lodash/isEmpty'

import ReviewActions from '../../reducer/ReviewRedux'
import user from '../../images/profile.png'

class Review extends Component {
  constructor () {
    super()
    this.state = {
      currentReview: '',
      reviewList: null,
      reviewRating: null,
      selectedStar: 5,
      filter: null,
      offset: 0,
      perPage: 5,
      currentPage: 0,
      elements: [],
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.reviews){
      const reviewRating = props.reviews.slice(props.reviews.length-2, props.reviews.length)
      const reviewList = props.reviews.slice(0, props.reviews.length-2)
      return {
        reviewList: reviewList,
        reviewRating: reviewRating[0].avg_star
      }
    }
    return null
  }

  componentDidMount() {
    const { reviewList, elements } = this.state
    if(reviewList && elements.length === 0) {
      this.setCurrentPage()
    }
  }

  handleReviewChange = (e) => {
    this.setState({
      currentReview: e.target.value
    })
  }

  handleFilterReview = (star) => {
    this.setState({
      filter: star,
      offset: 0,
      currentPage: 0
    }, () => {
      this.setCurrentPage()
    })
  }

  handleStarChange = (e) => {
    this.setState({
      selectedStar: e.target.value
    })
  }

  handleSendReview = async () => {
    const { selectedStar, currentReview } = this.state
    const { productId } = this.props
    // const email = await localforage.getItem('email')
    const name = await localforage.getItem('name')
    if (name) {
      const reviewPayload = {
        productId : productId,
        // name: email.split('@')[0],
        name: name,
        review: currentReview,
        star: parseInt(selectedStar)
      }
      this.props.insertReviewRequest(reviewPayload)
      window.alert("review posted")
      window.location.reload()
    } else {
      this.props.history.push('/login')
    }
  }

  handlePageClick = (data) => {
    const selectedPage = data.selected
    const offset = selectedPage * this.state.perPage
    this.setState({
      currentPage: selectedPage, offset: offset
    }, () => {
      this.setCurrentPage()
    })
  }

  setCurrentPage = () => {
    const { reviewList, offset, perPage, filter } = this.state
    if(!filter) {
      let elements = reviewList.slice(offset, offset + perPage)
      this.setState({
        elements: elements
      })
    } else {
      let elements = reviewList.filter(reviews => reviews.star === filter).slice(offset, offset + perPage)
      this.setState({
        elements: elements
      })
    }
  }

  render () {
    const { currentReview, reviewRating, reviewList, filter, selectedStar, currentPage, elements } = this.state
    return (
      <div className='margin-top-s'>
        {reviewList
        ? <div>
          {reviewRating} <AiFillStar/> ({reviewList.length} Reviews)
        </div>
        : <div>
          0 <AiFillStar/> (0 Reviews)
        </div>}
        <div className='flex margin-top-s'>
          <select 
            value={selectedStar} 
            onChange={this.handleStarChange} 
            className='review-radio cursor-pointer'
          >
            <option value={5}>5 Star</option>
            <option value={4}>4 Star</option>
            <option value={3}>3 Star</option>
            <option value={2}>2 Star</option>
            <option value={1}>1 Star</option>
          </select>
          <input type='text' value={currentReview} onChange={(e) => this.handleReviewChange(e)} className='commentbox' placeholder='Tulis ulasan disini' />
          <div className='sendcomment cursor-pointer' onClick={this.handleSendReview}>
            Send
          </div>
        </div>

        {!isEmpty(reviewList) 
          ? <div className='flex margin-top-s'>
            <div className='margin-top-xxxs color-grey'>
              Filter
            </div>
            <div className={(filter === 1) ? 'review-active-tab review-filter-button margin-left-s' : 'margin-left-s review-filter-button'} onClick={() => this.handleFilterReview(1)}>
              <AiFillStar/> 1
            </div>
            <div className={(filter === 2) ? 'review-active-tab review-filter-button margin-left-s' : 'margin-left-s review-filter-button'} onClick={() => this.handleFilterReview(2)}>
              <AiFillStar/> 2
            </div>
            <div className={(filter === 3) ? 'review-active-tab review-filter-button margin-left-s' : 'margin-left-s review-filter-button'} onClick={() => this.handleFilterReview(3)}>
              <AiFillStar/> 3
            </div>
            <div className={(filter === 4) ? 'review-active-tab review-filter-button margin-left-s' : 'margin-left-s review-filter-button'} onClick={() => this.handleFilterReview(4)}>
              <AiFillStar/> 4
            </div>
            <div className={(filter === 5) ? 'review-active-tab review-filter-button margin-left-s' : 'margin-left-s review-filter-button'} onClick={() => this.handleFilterReview(5)}>
              <AiFillStar/> 5
            </div>
          </div>
          : null
        }

        <div>
          {elements && elements.length > 0 && !filter
          ? elements.map((rev, index) => {
            return (
              <div key={index}className='review-container'>
                <div className='flex'>
                  <div className='comment-pic-container'>
                    {/* <div className='circle' /> */}
                    <img src={user} alt='no pic' height='65px'/>
                  </div>
                  <div style={{margin: '30px 0 0 15px'}}>
                    {rev.review_name}
                  </div>
                  <div className='review-star margin-top-xxs'>
                    {rev.star} <AiFillStar/>
                  </div>
                </div>
                <div style={{margin: '3px 0 20px 110px'}}>
                  {rev.review_comment}
                </div>
              </div>
            )
          })
        : (filter)
        ? elements.filter(reviews => reviews.star === filter).map((rev, index) => {
          return (
            <div key={index}className='review-container'>
              <div className='flex'>
                <div className='comment-pic-container'>
                  {/* <div className='circle' /> */}
                  <img src={user} alt='no pic' height='65px'/>
                </div>
                <div style={{margin: '30px 0 0 15px'}}>
                  {rev.review_name}
                </div>
                <div className='review-star margin-top-xxs'>
                  {rev.star} <AiFillStar/>
                </div>
              </div>
              <div style={{margin: '3px 0 20px 110px'}}>
                {rev.review_comment}
              </div>
            </div>
          )
        })
        : null
        }
        </div>
        {reviewList
        ? !filter 
          ? <ul>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={reviewList.length / 5}
            forcePage={currentPage}
            marginPagesDisplayed={1}
            pageRangeDisplayed={4}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages-pagination'}
            activeClassName={'active'}
          />
          </ul>
          : <ul>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={reviewList.filter(reviews => reviews.star === filter).length / 5}
            forcePage={currentPage}
            marginPagesDisplayed={1}
            pageRangeDisplayed={4}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages-pagination'}
            activeClassName={'active'}
          />
          </ul>
      : null
        }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    insertReviewRequest: (payload) => dispatch(ReviewActions.insertReviewRequest(payload))
  }
}

export default withRouter(connect(null, mapDispatchToProps)(Review))
