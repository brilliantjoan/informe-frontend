import React, { Component } from 'react'
import Select from 'react-select'
import { connect } from 'react-redux'

import ProductActions from '../../reducer/ProductRedux'
import CategoryActions from '../../reducer/CategoryRedux'

import ProductPeopleChoose from './ProductPeopleChoose'

// const productOption = [
//   { value: 101010, label: 'Samsung Galaxy S21 Ultra 5G' },
//   { value: 101011, label: 'Samsung Galaxy S20 FE' },
//   { value: 101013, label: 'Samsung Galaxy F41' },
//   { value: 101014, label: 'Samsung Galaxy Z Fold2 5G' },
//   { value: 101015, label: 'Samsung Galaxy Z Flip 5G' }
// ]

class ProductComparison extends Component {
  constructor () {
    super()
    this.state = {
      selectedProductOption: null,
      productDetail: null,
      product2Detail: null,
      productOption: [],
      itemList: [],
      insertProductOption: false,
      productCategory: null,
      getCategoryData: false
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.product2Detail) {
      returnObj = {
        ...returnObj,
        product2Detail: props.product2Detail
      }
    }
    
    if (props.productDetail) {
      returnObj = {
        ...returnObj,
        productDetail: props.productDetail
      }
    }

    if (props.categoryProductData) {
      returnObj = {
        ...returnObj,
        itemList: props.categoryProductData
      }
    }

    if (props.productInfo) {
      returnObj = {
        ...returnObj,
        productCategory: props.productInfo.Category
      }
    }

    return returnObj
  }

  componentDidUpdate() {
    const { itemList, insertProductOption, productCategory, getCategoryData } = this.state

    if(itemList.length !== 0 && insertProductOption === false) {
      let arrayItem = []
      itemList[0].map(item => {
        const arrValue = {
          value: item.ProductID,
          label: item.Title
        }
        return (
          arrayItem.push(arrValue)
        )
      })
      this.setState({insertProductOption: true, productOption: arrayItem})
    }
  }

  componentDidMount () {
    const {productCategory, getCategoryData} = this.state

    if(productCategory && getCategoryData === false) {
      this.props.categoryProductRequest(productCategory)
    }
    this.props.allProductRequest()
  }

  componentWillUnmount () {
    this.props.clearProduct()
  }

  handleProductChange = selectedOption => {
    this.setState({ selectedProductOption: selectedOption });
    this.props.product2Request(selectedOption.value)
    // console.log(`Option selected:`, selectedOption.value);
  };

  resetComparison = () => {
    this.setState({
      selectedProductOption: null
    })
  }

  renderProductSpec = () => {
    const { productDetail } = this.state
    const spec  = productDetail.Spesification
    // console.log('htmlres ', spec)
    if(productDetail.category === 'Handphone') {
      return <td dangerouslySetInnerHTML={{__html: spec}} />
    } else {
      return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
    }
  }

  renderProductSpec2 = () => {
    const { product2Detail } = this.state
    const spec  = product2Detail.Spesification
    if(product2Detail.category === 'Handphone') {
      return <td dangerouslySetInnerHTML={{__html: spec}} />
    } else {
      return <table><td dangerouslySetInnerHTML={{__html: "<table>" + spec +"</table>"}} /></table>
    }
  }

  render () {
    const { selectedProductOption, product2Detail, productDetail, productOption } = this.state;

    return (
      <div className='margin-top-xs'>
        Compare with

        <div>
          <div className='product-select-container cursor-pointer'>
            <Select
              value={selectedProductOption}
              onChange={(selectedProductOption) => this.handleProductChange(selectedProductOption)}
              options={productOption}
              placeholder='Choose a product...'
            />
          </div>
          <div className='reset-product cursor-pointer' onClick={this.resetComparison}>Reset</div>
        </div>

        {
          selectedProductOption === null
          ? <ProductPeopleChoose/>
          : <div className='flex'>
            {(productDetail)
                ? <div className='product-comparison-container'>
                  <div>
                    {productDetail.Title}
                  </div>
                  {this.renderProductSpec()}
                  {/* <table className='spec'>
                  {productDetail
                    ? Object.keys(productDetail).map((details, index) => {
                        if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto' && details !== 'HighestPrice' && details !== 'LowestPrice' && details !== 'Category') {
                          return (
                            <Fragment key={details}>
                              <thead>
                                <tr>
                                  <th>
                                    <div className='margin-top-s bold float-left margin-bottom-xxxs'>
                                      {details}
                                    </div>
                                  </th>
                                </tr>
                              </thead>
                              <tbody key={details}>
                                {typeof (productDetail[details]) === 'object'
                                  ? Object.keys(productDetail[details]).map((child, index) =>
                                    <tr key={child}>
                                      <td className='spec'>
                                        {child}
                                      </td>
                                      <td className='spec'>
                                        {productDetail[details][child]}
                                      </td>
                                    </tr>
                                    )
                                  : null}
                              </tbody>
                            </Fragment>
                          )
                        } else {
                          return null
                        }
                      }
                    
                      )
                    : null}
                  </table> */}
                  </div>
                  : null
              }
              {(product2Detail)
                ? <div className='product-comparison-container margin-left-m'>
                  <div>
                    {product2Detail.Title}
                  </div>
                  {this.renderProductSpec2()}
                  
                  {/* <table className='spec'>
                  {product2Detail
                    ? Object.keys(product2Detail).map((details, index) => {
                        if (details !== 'ProductId' && details !== 'Title' && details !== 'UrlPhoto' && details !== 'HighestPrice' && details !== 'LowestPrice' && details !== 'Category') {
                          return (
                            <Fragment key={details}>
                              <thead>
                                <tr>
                                  <th>
                                    <div className='margin-top-s bold float-left margin-bottom-xxxs'>
                                      {details}
                                    </div>
                                  </th>
                                </tr>
                              </thead>
                              <tbody key={details}>
                                {typeof (product2Detail[details]) === 'object'
                                  ? Object.keys(product2Detail[details]).map((child, index) =>
                                    <tr key={child}>
                                      <td className='spec'>
                                        {child}
                                      </td>
                                      <td className='spec'>
                                        {product2Detail[details][child]}
                                      </td>
                                    </tr>
                                    )
                                  : null}
                              </tbody>
                            </Fragment>
                          )
                        } else {
                          return null
                        }
                      }
                    
                      )
                    : null}
                  </table> */}
                  </div>
                  : null
              }
            </div>
            
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  product2Detail: state.product.product2Detail,
  productDetail: state.product.productDetail,
  categoryProductData: state.category.categoryProduct
})

const mapDispatchToProps = dispatch => {
  return {
    categoryProductRequest: (payload) => dispatch(CategoryActions.categoryProductRequest(payload)),
    product2Request: (payload) => dispatch(ProductActions.product2Request(payload)),
    allProductRequest: () => dispatch(ProductActions.allProductRequest()),
    clearProduct: () => dispatch(ProductActions.clearProduct())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductComparison)
