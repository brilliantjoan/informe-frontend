import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import Home from './Home'
import Login from './Login'
import SignUp from './SignUp'
import Category from './Category'
import Product from './Product'
import Compare from './Compare'
import Profile from './Profile'
import Upgrade from './Upgrade'
import Search from './Search'
import Wishlist from './Wishlist'

class App extends Component {
  render () {
    return (
      <>
        <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/signup' component={SignUp} />
            <Route exact path='/profile' component={Profile} />
            <Route exact path='/category' component={Category} />
            <Route exact path='/product' component={Product} />
            <Route exact path='/compare' component={Compare} />
            <Route exact path='/upgrade' component={Upgrade} />
            <Route exact path='/search' component={Search} />
            <Route exact path='/wishlist' component={Wishlist} />
          </Switch>
        </Router>
        <ToastContainer
          position='top-right'
          autoClose={3000}
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
          style={{ top: '70px' }}
        />
      </>
    )
  }
}

export default withRouter(App)
