import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import '../Product.css'
import Navbar from './Components/Navbar'
import Footer from './Components/Footer'

class Search extends Component {
  constructor () {
    super()
    this.state = {
      searchMessage: null,
      products: null
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.location.state.searchMessage) {
      returnObj = {
        ...returnObj,
        searchMessage: props.location.state.searchMessage
      }
    }

    if (props.searchData) {
      returnObj = {
        ...returnObj,
        products: props.searchData
      }
    }

    return returnObj
  }

  render () {
    const { searchMessage, products } = this.state
    return (
      <div>
        <Navbar />
        <div className='compare-container margin-top-xl'>
          <div className='terlaris-container'>
            <div className='margin-bottom-m'>
              HASIL PENCARIAN: {searchMessage.toUpperCase()}
            </div>
            <div className='flex-container flex-wrap'>
              {products
                ? products.map((prod, index) => {
                    let urlPhoto = ''
                    let name = ''
                    let productId = ''
                    Object.keys(prod).map((detail, index) => {
                      if (detail === 'Photo') {
                        urlPhoto = prod[detail]
                      }
                      if (detail === 'Title') {
                        name = prod[detail]
                      }
                      if (detail === 'ProductID') {
                        productId = prod[detail]
                      }
                      return null
                    })
                    if (urlPhoto && name) {
                      return (
                        <div className='product-border' key={name}>
                          <Link to={{
                            pathname: '/product',
                            state: {
                              productId: productId
                            }
                          }}
                          >
                            <div>
                              <img src={urlPhoto} alt='no pic' className='product-list-container' />
                            </div>
                          </Link>
                          <div className='product-text-container'>{name}</div>
                        </div>
                      )
                    } else { return null }
                  })

                : null}
            </div>
          </div>

        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  searchData: state.product.searchData
})

export default connect(mapStateToProps, null)(Search)
