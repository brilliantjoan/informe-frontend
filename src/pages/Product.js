import React, { Component, Fragment } from 'react'
import { AiFillStar } from 'react-icons/ai'
import { connect } from 'react-redux'
import localforage from 'localforage'

import Navbar from './Components/Navbar'
import Footer from './Components/Footer'
import ProductDescription from './Components/ProductDescription'
import ProductComparison from './Components/ProductComparison'
import PriceComparison from './Components/PriceComparison'
import Discussion from './Components/Discussion'
import Review from './Components/Review'
import '../Product.css'
import '../Home.css'

import ReviewActions from '../reducer/ReviewRedux'
import ProductActions from '../reducer/ProductRedux'

// import galaxyNote9 from '../images/galaxyNote9.jpg'
import heartRed from '../images/heart-red.jpeg'
import heartWhite from '../images/heart-white.jpeg'

class Product extends Component {
  constructor () {
    super()
    this.state = {
      tab: 'description',
      reviewRating: null,
      email: null,
      reviews: null,
      productDetail: null,
      productId: null,
      isWished: false,
      wishProduct: null,
      initialWish: true
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.review) {
      const reviewRating = props.review.slice(props.review.length-2, props.review.length)
      returnObj = {
        ...returnObj,
        reviews: props.review,
        reviewRating: reviewRating[0].avg_star
      }
    }

    if (props.product) {
      returnObj = {
        ...returnObj,
        productDetail: props.product
      }
    }

    if (props.location.state.productId) {
      returnObj = {
        ...returnObj,
        productId: props.location.state.productId
      }
    }

    if (props.wishData) {
      returnObj = {
        ...returnObj,
        wishProduct: props.wishData
      }
    }

    return returnObj
  }

  async componentDidMount () {
    const { productId } = this.state
    window.scrollTo(0, 0)

    const cacheEmail = await localforage.getItem('email')
    const userId = await localforage.getItem('cust_id')
    if (cacheEmail) {
      this.setState({ email: cacheEmail })
    }
    
    if (userId) {
      this.props.wishRequest(parseInt(userId))
    }

    const reviewPayload = {
      productId : productId,
      limit: 100,
      offset: 0
    }

    this.props.reviewRequest(reviewPayload)
    this.props.productRequest(productId)
    this.props.productPeopleChooseRequest(parseInt(productId))
  }
  
  componentDidUpdate() {
    const {initialWish, wishProduct, productDetail, isWished} = this.state

    if(initialWish && wishProduct && productDetail && !isWished) {
      this.initializeWish(wishProduct)
    }
  }

  componentWillUnmount () {
    this.props.clearProduct()
  }

  changeTab = (tab) => {
    this.setState({
      tab: tab
    })
  }

  onWishClick = async () => {
    const {isWished, productId} = this.state
    const userId = await localforage.getItem('cust_id')
    if(!isWished) {
      this.setState({
        isWished: true
      }, () => {
        const wishPayload = {
          userId: userId,
          productId: productId
        }  
        if(userId) {
          this.props.insertWishRequest(wishPayload)
        }
      })
    } else {
      this.setState({
        isWished: false
      }, () => {
        const wishPayload = {
          userId: userId,
          productId: productId
        }  
        if(userId) {
          this.props.deleteWishRequest(wishPayload)
        }
      })
    }
  }

  initializeWish = (wishData) => {
    const {productDetail} = this.state
    wishData.map((prod, index) => {
      let name = ''
      if(prod[0]) {
        Object.keys(prod[0]).map((detail, index) => {
          if (detail === 'Title') {
            name = prod[0][detail]
            if(name === productDetail.Title) {
              this.setState({isWished: true, initialWish: false})
            }
          }
          return null
        })
      }
      return null
    })
  }

  render () {
    const { tab, reviewRating, reviews, email, productDetail, isWished } = this.state
    return (
      <div>
        <Navbar />
        <div className='product-container flex margin-top-m'>
          {/* <div className='product-square' /> */}
          {productDetail
          ? <Fragment>
              <div className='margin-left-xs'>
                <img src={productDetail.Photo} alt='No Pic' className='product-image'/>
              </div>
              <div className='margin-left-s'>
                <div className='bold font-size-m'>{productDetail.Title.toUpperCase()}</div>
                {reviews && reviewRating
                ? <div className='margin-top-xxxs'>{reviewRating} <AiFillStar/>  ( {reviews.length-2} reviews )</div> 
                : <div className='margin-top-xxxs'> 0 <AiFillStar/> (0 Reviews) </div>}
                <div className='margin-top-xs underline cursor-pointer'>
                  {!isWished
                    ? <div onClick={() => this.onWishClick()}>
                        <img src={heartWhite} alt='No Pic' height='20px'/>
                      </div>
                    : <div onClick={() => this.onWishClick()}>
                        <img src={heartRed} alt='No Pic' height='20px'/>
                      </div>
                  }
                </div>
              </div>
            </Fragment>
          : null

          }
        </div>

        <div className='product-description-container'>
          <div className='margin-left-xs margin-right-m'>
            <div className='flex'>
              <div className='cursor-pointer'>
                <div className={(tab === 'description') ? 'active-tab' : ''}>
                  <div className='margin-left-s margin-right-s'>
                    <div className='product-tab bold' onClick={() => this.changeTab('description')}>
                      Specification
                    </div>
                  </div>
                </div>
              </div>

              <div className={(tab === 'compare') ? 'active-tab' : ''}>
                <div className='margin-left-s margin-right-s'>
                  <div className='product-tab bold' onClick={() => this.changeTab('compare')}>
                    Comparison
                  </div>
                </div>
              </div>

              <div className={(tab === 'price') ? 'active-tab' : ''}>
                <div className='margin-left-s margin-right-s'>
                  <div className='product-tab bold' onClick={() => this.changeTab('price')}>
                    Price
                  </div>
                </div>
              </div>

              <div className={(tab === 'discussion') ? 'active-tab' : ''}>
                <div className='margin-left-s margin-right-s'>
                  <div className='product-tab bold' onClick={() => this.changeTab('discussion')}>
                    Discussion
                  </div>
                </div>
              </div>

              <div className={(tab === 'review') ? 'active-tab' : ''}>
                <div className='margin-left-s margin-right-s'>
                  <div className='product-tab bold' onClick={() => this.changeTab('review')}>
                    Review
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div className='margin-left-xs border-bottom-grey' />
          <div className='product-detail-container'>
            {
              tab === 'description'
              ? <ProductDescription productDetail={productDetail}/>
              : null
            }
            {
              tab === 'compare'
              ? <ProductComparison productInfo={productDetail}/>
              : null
            }
            {
              tab === 'price'
              ? <PriceComparison productId={productDetail.ProductID} productName={productDetail.Title}/>
              : null
            }
            {
              tab === 'discussion'
              ? <Discussion productId={productDetail.ProductID}/>
              : null
            }
            {
              tab === 'review'
              ? <Review reviewRating={reviewRating} reviews={reviews} email={email} productId={productDetail.ProductID}/>
              : null
            }
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  review: state.review.review,
  product: state.product.productDetail,
  wishData: state.product.wishData
})

const mapDispatchToProps = dispatch => {
  return {
    reviewRequest: (payload) => dispatch(ReviewActions.reviewRequest(payload)),
    productRequest: (payload) => dispatch(ProductActions.productRequest(payload)),
    productPeopleChooseRequest: (payload) => dispatch(ProductActions.productPeopleChooseRequest(payload)),
    wishRequest: (payload) => dispatch(ProductActions.wishRequest(payload)),
    insertWishRequest: (payload) => dispatch(ProductActions.insertWishRequest(payload)),
    deleteWishRequest: (payload) => dispatch(ProductActions.deleteWishRequest(payload)),
    clearProduct: () => dispatch(ProductActions.clearProduct())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product)
