import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import localforage from 'localforage'

import Footer from './Components/Footer'

import UserActions from '../reducer/UserRedux'

class Login extends Component {
  constructor () {
    super()
    this.state = {
      email: '',
      password: '',
      loginData: null,
      // isPushed: false,
      loginEmail: null,
      loginSuccess: null,
      sendLogin: false
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}

    if(props.fetchingLogin === false && props.isLogin === false && props.user === 'failed') {
      returnObj = {
        ...returnObj,
        loginData: null,
        loginSuccess: false
      }
    }

    return returnObj
  }

  async componentDidUpdate() {
    const { loginSuccess, sendLogin, loginData } = this.state
    const { user, isLogin } = this.props
    // if(loginData && !isPushed) {
    //   console.log('logindata ',loginData)
    // }
    


    if(!loginData && user && isLogin === true) {
      // console.log('update asdfg', loginData, user)
      const data = user
      await localforage.setItem('token', data.token)
      await localforage.setItem('email', data.email)
      await localforage.setItem('cust_id', data.id)
      await localforage.setItem('name', data.user_name)
      await localforage.setItem('role', data.role)
      this.setState ({
        loginData: data,
        loginSuccess: true
      })

      if(sendLogin === true) {

      // const token = await localforage.getItem('token')
      // const email = await localforage.getItem('email')
      // const custId = await localforage.getItem('cust_id')
        if(data.token && data.email && data.custId) {
          toast.success('Login Success')
          this.props.history.push('/')
        }
      }
    }

    const token = await localforage.getItem('token')
    const email = await localforage.getItem('email')
    const custId = await localforage.getItem('cust_id')
    if(loginSuccess === false && sendLogin === true && isLogin === false) {
      this.setState({
        sendLogin: false
      })
      this.props.cleanRegister()
      toast.error('Invalid credentials')  
    } else if(loginSuccess === true && sendLogin === true && isLogin === true) {
      if(token && email && custId) {
        toast.success('Login Success')
        this.props.history.push('/')
      }
    }
  }

  handleEmailChange = event => {
    this.setState({
      email: event.target.value
    })
  }

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value
    })
  }

  handleLogin = () => {
    const { email, password } = this.state
    const payload = {
      email: email,
      password: password
    }
    this.setState({
      sendLogin: true
    }, () => {
      this.props.requestUserLogin(payload)
    })
    
  }

  render () {
    const { email, password } = this.state
    return (
      <div className='background-light-grey'>
        <div className='login-header'>
          <Link to='/' className='text-decoration-none'>
            <div className='login-logo'>
              <label className='color-custom cursor-pointer'>
                InforMe
              </label>
            </div>
          </Link>
        </div>
        <div className='login-container'>
          <div className='login-inner-container'>
            <div className='padding-top-m login-text'>
              Sign in
            </div>
            <div className='login-subtext'>
              sign in to unlock more features
            </div>

            <input type='text' name='email' placeholder='Email' className='login-input' value={email} onChange={this.handleEmailChange}/>
            <input type='password' name='password' placeholder='Password' className='login-input margin-top-s' value={password} onChange={this.handlePasswordChange}/>
            {/* <div className='margin-top-xs margin-left-xxs'>
              <input type='checkbox' />
              <label className='color-grey'>
                Remember Me
              </label>
            </div> */}
            <div className='margin-top-s margin-left-xxs' onClick={this.handleLogin}>
                <button className='signin-button cursor-pointer'>Sign in</button>
            </div>

            {/* <div className='margin-top-s margin-left-xxs align-center'>
              Forgot password?
            </div> */}

            <div>
              <Link to='/signup' className='text-decoration-none'>
                <div className='margin-top-s margin-left-xxs align-center'>
                  Create account
                </div>
              </Link>
            </div>

          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.userData,
  isLogin: state.user.isLogin,
  fetchingLogin: state.user.loginFetch
})

const mapDispatchToProps = dispatch => {
  return {
    requestUserLogin: (payload) => dispatch(UserActions.loginRequest(payload)),
    cleanRegister: () => dispatch(UserActions.cleanRegister())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
