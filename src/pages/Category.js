import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'

import Navbar from './Components/Navbar'

import CategoryActions from '../reducer/CategoryRedux'

class Category extends Component {
  constructor () {
    super()
    this.state = {
      categoryList: null,
      // itemList: ['Nokia', 'Samsung S8', 'Samsung S9', 'iphone 10', 'iphone 8', 'iphone 6', 'A', 'B', 'C', 'D', 'E', 'F'],
      itemList: null,
      currentCategory: '',
      currentSort: 'name',
      // filter: null,
      offset: 0,
      perPage: 8,
      currentPage: 0,
      elements: [],
      insertCat: false
    }
  }

  static getDerivedStateFromProps (props, state) {
    // if (props.category) {
    //   return {
    //     categoryList: props.category
    //   }
    // }
    // return null
    let returnObj = {}

    if (props.category) {
      returnObj = {
        ...returnObj,
        categoryList: props.category
      }
    }

    if (props.categoryProduct) {
      returnObj = {
        ...returnObj,
        itemList: props.categoryProduct,
        insertCat: true
      }
    }
    return returnObj
  }

  componentDidMount () {
    const { location } = this.props
    window.scrollTo(0, 0)
    const category_product = location.query
    if (category_product) {
      this.setState({
        currentCategory: category_product
      }, () => {
        this.props.categoryProductRequest(category_product)
      })
    }
    this.props.categoryRequest()
    
  }

  componentDidUpdate() {
    const { itemList, elements, insertCat } = this.state

    if(itemList && elements.length === 0 && insertCat) {
      this.setCurrentPage()
      this.props.clearCategoryProduct()
      this.setState({insertCat: false})
    }
  }

  onCategoryChange = e => {
    this.setState({
      currentCategory: e.target.value,
      elements: []
    })
    this.props.categoryProductRequest(e.target.value)
  }

  onSortChange = e => {
    this.setState({
      currentSort: e.target.value
    }, () => {
      this.setCurrentPage()
    })
  }

  handlePageClick = (data) => {
    const selectedPage = data.selected
    const offset = selectedPage * this.state.perPage
    this.setState({
      currentPage: selectedPage, offset: offset
    }, () => {
      this.setCurrentPage()
    })
  }

  setCurrentPage = () => {
    const { itemList, offset, perPage, currentSort } = this.state
    if(!currentSort) {
      let elements = itemList.slice(offset, offset + perPage)
      this.setState({
        elements: elements
      })
    } else {
      if(currentSort === 'name') {
        let elements = itemList[0].sort((a,b) => (a.Title > b.Title) ? 1 : ((b.Title > a.Title) ? -1 : 0)).slice(offset, offset + perPage)
        this.setState({
          elements: elements
        })
      } else if(currentSort === 'price') {
        let elements = itemList[0].sort((a,b) => b.LowestPrice - a.LowestPrice).slice(offset, offset + perPage)
        this.setState({
          elements: elements
        })
      }
    }
  }

  render () {
    const { categoryList, itemList, currentCategory, currentSort, currentPage, elements } = this.state
    return (
      <div>
        <Navbar />
        <div className='category-sidenav'>
          <div className='margin-left-s' style={{ fontWeight: 'bold' }}>
            CATEGORIES
          </div>
          {categoryList
            ? categoryList.map(categories => {
              if (categories) {
                return (
                  <div key={categories.CategoryProduct} className='margin-top-s margin-left-s'>
                    <input type='radio' name={categories.CategoryProduct} value={categories.CategoryProduct} 
                      onChange={this.onCategoryChange} checked={categories.CategoryProduct === currentCategory}/>
                    <span style={{ 'marginLeft': '5px' }}>{categories.CategoryProduct}</span>
                    <br />
                  </div>
                )
              } else {
                return null
              }
            })
            : null
            }

            <div className='margin-top-l'>
              <div className='margin-left-s' style={{ fontWeight: 'bold'}}>
                SORT BY
              </div>

              <div className='margin-top-s margin-left-s'>
                <input type='radio' name='name' value='name' 
                  onChange={this.onSortChange} checked={currentSort === 'name'}/>
                <span style={{ 'marginLeft': '5px' }}>Name</span>
                <br />
              </div>
              <div className='margin-top-s margin-left-s'>
                <input type='radio' name='price' value='price' 
                  onChange={this.onSortChange} checked={currentSort === 'price'}/>
                <span style={{ 'marginLeft': '5px' }}>Price</span>
                <br />
              </div>
            </div>
        </div>
        <div className='category-main margin-top-m'>
          <div className='category-title'>
            PRODUCTS
          </div>
          <div className='flex flex-wrap'>
            {elements
              ? elements.map(items => {
                let urlPhoto = ''
                let name = ''
                let productId = ''
                let lowestPrice = ''
                Object.keys(items).map((detail, index) => {
                  if (detail === 'Photo') {
                    urlPhoto = items[detail]
                  }
                  if (detail === 'Title') {
                    name = items[detail]
                  }
                  if (detail === 'ProductID') {
                    productId = items[detail]
                  }
                  if (detail === 'LowestPrice') {
                    lowestPrice = items[detail].toString()
                    if (lowestPrice.length > 4) {
                      lowestPrice = lowestPrice.substring(0, lowestPrice.length - 3) + '.' + lowestPrice.substring(lowestPrice.length - 3, lowestPrice.length)
                    }
                    if (lowestPrice.length > 7) {
                      lowestPrice = lowestPrice.substring(0, lowestPrice.length - 7) + '.' + lowestPrice.substring(lowestPrice.length - 7, lowestPrice.length)
                    }
                    if (lowestPrice.length > 10) {
                      lowestPrice = lowestPrice.substring(0, lowestPrice.length - 11) + '.' + lowestPrice.substring(lowestPrice.length - 11, lowestPrice.length)
                    }
                  }
                  return null
                })
                if (urlPhoto && name) {
                  return (
                    <div className='product-category-border' key={name}>
                      <Link to={{
                        pathname: '/product',
                        state: {
                          productId: productId
                        }
                      }}
                      >
                        <div>
                          <img src={urlPhoto} alt='no pic' className='product-list-container' />
                        </div>
                      </Link>
                      <div className='product-text-container bold'>{name}</div>
                      <div className='font-size-xxs align-center'> <br />Lowest Price: Rp {lowestPrice}</div>
                    </div>
                  )
                } else { return null }
                // if (items) {
                //   return (
                //     <div key={items.ProductId} >
                //       <Link to='/product'>
                //         <div className='product-square-2'>
                //           {items}
                //         </div>
                //       </Link>
                //     </div>
                //   )
                // } else {
                //   return null
                // }
              })
              : null
            }
          </div>
          {itemList
        ?  <ul>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={itemList[0].length / 8}
            forcePage={currentPage}
            marginPagesDisplayed={1}
            pageRangeDisplayed={8}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages-pagination'}
            activeClassName={'active'}
          />
          </ul>
      : null
        }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  category: state.category.category,
  categoryProduct: state.category.categoryProduct
})

const mapDispatchToProps = dispatch => {
  return {
    categoryRequest: () => dispatch(CategoryActions.categoryRequest()),
    categoryProductRequest: (payload) => dispatch(CategoryActions.categoryProductRequest(payload)),
    clearCategoryProduct: () => dispatch(CategoryActions.clearCategoryProduct())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)
