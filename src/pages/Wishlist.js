import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import localforage from 'localforage'

import '../Product.css'
import Navbar from './Components/Navbar'

import ProductActions from '../reducer/ProductRedux'

class Wishlist extends Component {
  constructor () {
    super()
    this.state = {
      products: null
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.product) {
      returnObj = {
        ...returnObj,
        products: props.product
      }
    }
    return returnObj
  }

  async componentDidMount () {
    const custId = await localforage.getItem('cust_id')
    if (custId) {
      this.props.wishRequest(parseInt(custId))
    }
  }

  render () {
    const { products } = this.state
    return (
      <div>
        <Navbar />
        <div className='margin-top-l'>
          <div className='align-center font-size-m bold'>
            YOUR WISHLIST
          </div>
          <div className='wishlist-container'>
            <div className='flex-container flex-wrap'>
              {products
                ? products.map((prod, index) => {
                    let urlPhoto = ''
                    let name = ''
                    let productId = ''
                    let lowestPrice = ''
                    if (prod[0]) {
                      Object.keys(prod[0]).map((detail, index) => {
                        if (detail === 'Photo') {
                          urlPhoto = prod[0][detail]
                        }
                        if (detail === 'Title') {
                          name = prod[0][detail]
                        }
                        if (detail === 'ProductID') {
                          productId = prod[0][detail]
                        }
                        if (detail === 'LowestPrice') {
                          lowestPrice = prod[0][detail].toString()
                          if (lowestPrice.length > 4) {
                            lowestPrice = lowestPrice.substring(0, lowestPrice.length - 3) + '.' + lowestPrice.substring(lowestPrice.length - 3, lowestPrice.length)
                          }
                          if (lowestPrice.length > 7) {
                            lowestPrice = lowestPrice.substring(0, lowestPrice.length - 7) + '.' + lowestPrice.substring(lowestPrice.length - 7, lowestPrice.length)
                          }
                          if (lowestPrice.length > 10) {
                            lowestPrice = lowestPrice.substring(0, lowestPrice.length - 11) + '.' + lowestPrice.substring(lowestPrice.length - 11, lowestPrice.length)
                          }
                        }
                        return null
                      })
                    }
                    if (urlPhoto && name) {
                      return (
                        <div className='flex margin-right-s wish-border' key={name}>
                          <div className='margin-left-m'>
                            <img src={urlPhoto} alt='no pic' className='wish-image-container' />
                          </div>
                          <div className='margin-left-m margin-right-xxs margin-top-xxs margin-bottom-xxxs width-100'>
                            <div className='product-wish-container'>{name}</div><br />
                            <div>
                              Lowest Price: Rp {lowestPrice}
                            </div>
                            <Link
                              to={{
                                pathname: '/product',
                                state: {
                                  productId: productId
                                }
                              }}
                              style={{ textDecoration: 'none' }}
                            >
                              <div className='product-wish-container margin-top-l'>
                                <div className='wish-button'>
                                  Go to Page
                                </div>
                              </div>
                            </Link>
                          </div>
                        </div>
                      )
                    } else { return null }
                  })

                : null}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  product: state.product.wishData,
  categoryProduct: state.category.categoryProduct
})

const mapDispatchToProps = dispatch => {
  return {
    wishRequest: (payload) => dispatch(ProductActions.wishRequest(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Wishlist)
