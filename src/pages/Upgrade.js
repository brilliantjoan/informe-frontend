import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import isEmpty from 'lodash/isEmpty'

import '../Home.css'
import Navbar from './Components/Navbar'
import Footer from './Components/Footer'
import Select from 'react-select'

import CategoryActions from '../reducer/CategoryRedux'
import ProductActions from '../reducer/ProductRedux'

const categoryOption = [
  { value: 'Handphone', label: 'Handphone' },
  { value: 'Laptop', label: 'Laptop' }
]

// const productOption = [
//   // { value: 'galaxy note 9', label: 'Galaxy note 9' },
//   // { value: 'iphone 7', label: 'Iphone 7' },
//   // { value: 'samsung s20', label: 'Samsung S20' }
// ]

class Upgrade extends Component {
  constructor () {
    super()
    this.state = {
      selectedCategory: null,
      selectedProduct: null,
      productOption:[],
      searchClick: false,
      products: null,
      itemList: [],
      insertProductOption: false,
      budget: ''
    }
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    // if (props.category) {
    //   returnObj = {
    //     ...returnObj,
    //     categories: props.category
    //   }
    // }
    if (props.category) {
      returnObj = {
        ...returnObj,
        categoryList: props.category
      }
    }

    if (props.product) {
      returnObj = {
        ...returnObj,
        products: props.product
      }
    }

    if (props.categoryProduct) {
      // console.log('categoryProduct', props.categoryProduct)
      returnObj = {
        ...returnObj,
        itemList: props.categoryProduct
      }
    }

    return returnObj
  }

  componentDidUpdate() {
    const { itemList, insertProductOption } = this.state
    if(itemList.length !== 0 && insertProductOption === false) {
      let arrayItem = []
      itemList[0].map(item => {
        const arrValue = {
          value: item.ProductID,
          label: item.Title
        }
        return (
          arrayItem.push(arrValue)
        )
      })
      this.setState({insertProductOption: true, productOption: arrayItem})
    }
  }

  handleCategoryChange = (selectedCategory) => {
    this.setState({selectedCategory: selectedCategory, insertProductOption: false, itemList: [], productOption: []})
    this.props.categoryProductRequest(selectedCategory.value)
  }

  handleProductChange = (selectedProduct) => {
    this.setState({selectedProduct: selectedProduct})
  }

  handleBudgetChange = (e) => {
    this.setState({budget: e.target.value})
  }


  handleSearchClick = () => {
    const {selectedProduct, budget} = this.state
    // this.props.allProductRequest()
    const upgradePayload = {
      productId: selectedProduct.value,
      budget: budget
    }
    this.props.upgradeProductRequest(upgradePayload)
    this.setState({searchClick: true})
  }

  render () {
    const { selectedCategory, selectedProduct, searchClick, products, productOption, budget } = this.state
    return (
      <div>
        <Navbar />
        <div className='upgrade-container margin-top-xl'>
          <div className='bold'>UPGRADE PRODUCT RECOMMENDATION</div>
          <table className='margin-top-xs'>
            <tbody>
              <tr>
                <td className='padding-top-xxs'>Category</td>
                <td><Select
                      value={selectedCategory}
                      onChange={(selectedCategory) => this.handleCategoryChange(selectedCategory)}
                      options={categoryOption}
                      placeholder='Choose a category...'
                    /></td>
              </tr>

              <tr>
              <td className='padding-top-xxs'>Owned Gadget</td>
              <td><Select
                      value={selectedProduct}
                      onChange={(selectedProduct) => this.handleProductChange(selectedProduct)}
                      options={productOption}
                      placeholder='Choose a product...'
                    /></td>
              </tr>

              <tr>
                <td className='padding-top-xxs'>Budget</td>
                <td>
                  <input type='text' className='inputText' style={{ width: '400px' }} onChange={this.handleBudgetChange}/>
                </td>
              </tr>

              <tr>
                { productOption.length > 0 && budget !== '' && !isEmpty(productOption)
                  ?<td>
                  <button className='btn-primary cursor-pointer' onClick={this.handleSearchClick}>Search</button>
                </td>
                  : <td>
                  <button className='btn-primary cursor-pointer' disabled>Search</button>
                </td>
                }
              </tr>
            </tbody>
          </table>

          {searchClick
          ? <div>
              <div>
                Recommendation
              </div>
              <div className='flex-container flex-wrap'>
            {products
              ? products.map((prod, index) => {
                  let urlPhoto = ''
                  let name = ''
                  let productId = ''
                  Object.keys(prod).map((detail, index) => {
                    if (detail === 'Photo') {
                      urlPhoto = prod[detail]
                    }
                    if (detail === 'Title') {
                      name = prod[detail]
                    }
                    if (detail === 'ProductID') {
                      productId = prod[detail]
                    }
                    return null
                  })
                  if (urlPhoto && name) {
                    return (
                      <div className='product-border' key={name}>
                        <Link to={{
                          pathname: '/product',
                          state: {
                            productId: productId
                          }
                        }}
                        >
                          <div>
                            <img src={urlPhoto} alt='no pic' className='product-list-container' />
                          </div>
                        </Link>
                        <div className='product-text-container'>{name}</div>
                      </div>
                    )
                  } else { return 'No Product Found' }
                })

              : 'No Product Found'}
          </div>
            </div>
          : null
          }
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  category: state.category.category,
  categoryProduct: state.category.categoryProduct,
  product: state.product.upgradeProduct
})

const mapDispatchToProps = dispatch => {
  return {
    categoryRequest: () => dispatch(CategoryActions.categoryRequest()),
    allProductRequest: () => dispatch(ProductActions.allProductRequest()),
    categoryProductRequest: (payload) => dispatch(CategoryActions.categoryProductRequest(payload)),
    upgradeProductRequest: (payload) => dispatch(ProductActions.upgradeProductRequest(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Upgrade)