import React, { Component } from 'react'
import { Slide } from 'react-slideshow-image'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import '../Home.css'
import 'react-slideshow-image/dist/styles.css'
import img1 from '../images/samsung-banner.jpg'
import img2 from '../images/asus-banner.jpg'
import img3 from '../images/iphone-banner.jpg'
import Navbar from './Components/Navbar'
import Footer from './Components/Footer'
// import galaxyNote9 from '../images/galaxyNote9.jpg'
// import ps5 from '../images/ps5.jpg'

// import Handphone from '../images/phoneCategory.jpg'
// import Laptop from '../images/laptopCategory.jpg'
// import headsetCategory from '../images/headsetCategory.jpg'

import CategoryActions from '../reducer/CategoryRedux'
import ProductActions from '../reducer/ProductRedux'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      slideIndex: 1,
      token: null,
      categories: null,
      email: null,
      products: null
    }
  }

  componentDidMount () {
    window.scrollTo(0, 0)
    this.props.categoryRequest()
    this.props.popularProductRequest()
  }

  static getDerivedStateFromProps (props, state) {
    let returnObj = {}
    if (props.user) {
      window.location.reload()
      returnObj = {
        ...returnObj,
        token: props.user.token,
        email: props.user.email
      }
    }

    if (props.category) {
      returnObj = {
        ...returnObj,
        categories: props.category
      }
    }

    if (props.product) {
      returnObj = {
        ...returnObj,
        products: props.product
      }
    }
    return returnObj
  }

  render () {
    const slideImages = [
      img1,
      img2,
      img3
    ]
    const { categories, products } = this.state
    return (
      <div className='home'>
        <Navbar />
        <div className='slide-container'>
          <Slide easing='ease'>
            <div className='each-slide'>
              <div style={{ backgroundImage: `url(${slideImages[0]})` }} />
            </div>
            <div className='each-slide'>
              <div style={{ backgroundImage: `url(${slideImages[1]})` }} />
            </div>
            <div className='each-slide'>
              <div style={{ backgroundImage: `url(${slideImages[2]})` }} />
            </div>
          </Slide>
        </div>

        <div className='category-container'>
          <div className='bold'>
            CATEGORY
          </div>
          <div className='flex-container flex'>
            {/* <div className='category-border'>
              <Link to={{ pathname: '/category', query: 'Handphone' }}>
                <img src={phoneCategory} alt='no pic' className='product-square-container' />
              </Link>
              <div className='category-text-container'>
                HANDPHONE
              </div>
            </div>
            <div className='category-border'>
              <Link to={{ pathname: '/category', query: 'Laptop' }}>
                <img src={laptopCategory} alt='no pic' className='product-square-container' />
              </Link>
              <div className='category-text-container'>
                LAPTOP
              </div>
            </div>
            <div className='category-border'>
              <Link to={{ pathname: '/category', query: 'Headset' }}>
                <img src={headsetCategory} alt='no pic' className='product-square-container' />
              </Link>
              <div className='category-text-container'>
                HEADSET
              </div>
            </div> */}
            {categories
              ? categories.map((category, i) => {
                  const imagePath = require('../images/' + category.CategoryProduct + '.jpg').default
                  return (
                    <div key={i} className='category-border'>
                      <Link to={{ pathname: '/category', query: category.CategoryProduct }}>
                        <img src={imagePath} alt='no pic' className='product-category-container' />
                      </Link>
                      <div className='category-text-container'>
                        {category.CategoryProduct.toUpperCase()}
                      </div>
                    </div>
                  )
                })
              : null}
            {/* <div className='flex-container-product' />
            <div className='flex-container-product' />
            <div className='flex-container-product' /> */}
          </div>
        </div>
        <div className='terlaris-container'>
          <div className='bold'>
            POPULAR PRODUCT
          </div>
          <div className='flex-container flex-wrap'>
            {products
              ? products.map((prod, index) => {
                  let urlPhoto = ''
                  let name = ''
                  let productId = ''
                  if (prod[0]) {
                    Object.keys(prod[0]).map((detail, index) => {
                      if (detail === 'Photo') {
                        urlPhoto = prod[0][detail]
                      }
                      if (detail === 'Title') {
                        name = prod[0][detail]
                      }
                      if (detail === 'ProductID') {
                        productId = prod[0][detail]
                      }
                      return null
                    })
                    if (urlPhoto && name) {
                      return (
                        <div className='product-border' key={name}>
                          <Link to={{
                            pathname: '/product',
                            state: {
                              productId: productId
                            }
                          }}
                          >
                            <div>
                              <img src={urlPhoto} alt='no pic' className='product-list-container' />
                            </div>
                          </Link>
                          <div className='product-text-container bold'>{name}</div>
                        </div>
                      )
                    } else return null
                  } else return null
                })

              : null}
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.userData,
  category: state.category.category,
  product: state.product.popularProductData
})

const mapDispatchToProps = dispatch => {
  return {
    categoryRequest: () => dispatch(CategoryActions.categoryRequest()),
    popularProductRequest: () => dispatch(ProductActions.popularProductRequest())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
