import React, { Component } from 'react'
import { connect } from 'react-redux'
import localforage from 'localforage'
import { toast } from 'react-toastify'

import Navbar from './Components/Navbar'

import Footer from './Components/Footer'
import user from '../images/profile.png'

import UserActions from '../reducer/UserRedux'

class Profile extends Component {
  constructor () {
    super()
    this.state = {
      email: null,
      name: null,
      password: '******',
      userId: null,
      editName: false,
      editEmail: false,
      editPassword: false,
      changeName: null,
      changeEmail: null,
      changePassword: null
    }
  }

  async componentDidMount () {
    const email = await localforage.getItem('email')
    const name = await localforage.getItem('name')
    const userId = await localforage.getItem('cust_id')
    if (email) {
      this.setState({
        email: email
      })
    }

    if (name) {
      this.setState({
        name: name
      })
    }

    if (userId) {
      this.setState({
        userId: userId
      })
    }
  }

  nameEdit = () => {
    this.setState({editName: true})
  }

  emailEdit = () => {
    this.setState({editEmail: true})
  }

  passwordEdit = () => {
    this.setState({editPassword: true})
  }

  handleNameChange = (e) => {
    this.setState({changeName: e.target.value})
  }
  
  handleEmailChange = (e) => {
    this.setState({changeEmail: e.target.value})
  }

  handlePasswordChange = (e) => {
    this.setState({changePassword: e.target.value})
  }

  handleSendData = async(choose) => {
    const { changeName, changeEmail, changePassword, userId } = this.state

    const profilePayload = {
      user_id: userId,
      user_name: '',
      email: '',
      password: ''
    }
    let isValid = false
    if(choose === 'name') {
      if(!changeName) {
        toast.error('Name must be filled!')
      } else {
        profilePayload.user_name = changeName
        isValid = true
      }
    } else if(choose === 'email') {
      if(!changeEmail) {
        toast.error('Email must be filled!')
      } else {
        profilePayload.email = changeEmail
        isValid = true
      }
    } else if(choose === 'password') {
      if(!changePassword) {
        toast.error('Password must be filled!')
      } else {
        profilePayload.password = changePassword
        isValid = true
      }
    }

    if(isValid === true) {
      if (window.confirm(`Are you sure you want to edit this ${choose}?`)) {
        toast.success('Success!')
        this.props.updateProfileRequest(profilePayload)
        // await localforage.getItem('token').then((token) => {
        //   localforage.setItem('token', token)
        // })
        if(choose === 'email') {
          await localforage.getItem('email').then(() => {
            localforage.setItem('email', changeEmail)
            window.location.reload()
          })
        } else if(choose === 'name') {
          await localforage.getItem('name').then(() => {
            localforage.setItem('name', changeName)
            window.location.reload()
          })
        }
        // await localforage.getItem('cust_id').then((cust_id) => {
        //   localforage.setItem('cust_id', cust_id)
        // })
        // await localforage.getItem('role').then((role) => {
        //   localforage.setItem('role', role)
        // })
        // this.props.logout()
        // this.props.history.push('/login')
      }
    }
  } 

  render () {
    const { email, name, password, editName, editPassword, editEmail, changeName, changePassword, changeEmail } = this.state
    return (
      <div className='background-light-grey'>
        <Navbar />
        <div className='margin-top-m'>
          <div className='profile-sidenav'>
            <div className='margin-left-s'>
              <div className='profile-sidenav-inner'>
                {/* <div className='profile-sidenav-inner-text'> */}
                Account Information
                {/* </div> */}
                {/* <div className='padding-top-s'>
                  Security
                </div> */}
              </div>
            </div>
          </div>
          <div className='profile-main'>
            <div className='profile-main-inner'>
              <div className='align-center'>
                {/* <div className='profile-circle' /> */}
                <img src={user} alt='no pic' height='250px' />
                <div className='margin-top-s profile-name'>
                  Welcome Brilliant!
                </div>
              </div>
              <div className='margin-left-xs profile-title'>
                Account Info
              </div>
              <div className='profile-subtitle' />
              <div className='margin-top-s'>
                <table>
                  <tbody>
                    <tr>
                      <td className='bigger-font bold'>
                        Name
                      </td>
                      <td className='bigger-font'>
                        <div className='margin-left-s'>
                          {name}
                          <span className='margin-left-xxs underline edit-text cursor-pointer' onClick={() => this.nameEdit()}>
                            edit
                          </span>
                        </div>
                      </td>
                      <td>
                      {editName 
                        ? <div className='flex'>
                            <input type='text' value={changeName} onChange={(e) => this.handleNameChange(e)} className='profilebox' placeholder='Change name to'/>
                            <div className='sendprofile' onClick={() => this.handleSendData('name')}>
                              Send
                            </div>
                          </div>
                        : null
                      }
                      </td>
                    </tr>
                    <tr>
                      <td className='bigger-font bold'>
                        Emailname
                      </td>
                      <td className='bigger-font'>
                        <div className='margin-left-s'>
                          {email}
                          <span className='margin-left-xxs underline edit-text cursor-pointer' onClick={() => this.emailEdit()}>
                            edit
                          </span>
                        </div>
                      </td>
                      <td>
                      {editEmail
                        ? <div className='flex'>
                            <input type='text' value={changeEmail} onChange={(e) => this.handleEmailChange(e)} className='profilebox' placeholder='Change email to'/>
                            <div className='sendprofile' onClick={() => this.handleSendData('email')}>
                              Send
                            </div>
                          </div>
                        : null
                      }
                      </td>
                    </tr>
                    <tr>
                      <td className='bigger-font bold margin-bottom-m'>
                        Password
                      </td>
                      <td className='bigger-font'>
                        <div className='margin-left-s'>
                          {password}
                          <span className='margin-left-xxs underline edit-text cursor-pointer' onClick={() => this.passwordEdit()}>
                            change
                          </span>
                        </div>
                      </td>
                      <td>
                      {editPassword
                        ? <div className='flex'>
                            <input type='password' value={changePassword} onChange={(e) => this.handlePasswordChange(e)} className='profilebox' placeholder='Change password to'/>
                            <div className='sendprofile' onClick={() => this.handleSendData('password')}>
                              Send
                            </div>
                          </div>
                        : null
                      }
                      </td>
                    </tr> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  response: state.user.updateData
})

const mapDispatchToProps = dispatch => {
  return {
    updateProfileRequest: (payload) => dispatch(UserActions.updateProfileRequest(payload)),
    logout: () => dispatch(UserActions.logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
