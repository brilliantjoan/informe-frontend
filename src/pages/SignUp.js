import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'

import Footer from './Components/Footer'
import UserActions from '../reducer/UserRedux'

class SignUp extends Component {
  constructor () {
    super()
    this.state = {
      name: '',      
      email: '',
      password: '',
      confirmPassword: '',
      registerMessage: null
    }
  }

  static getDerivedStateFromProps (props, state) {
    if(props.user) {
      return {
        registerMessage: props.user
      }
    }
    return null
  }

  componentDidUpdate() {
    const { registerMessage } = this.state
    if(registerMessage && typeof registerMessage === 'object') {
      toast.success('Success!')
      this.props.cleanRegister()
      this.props.history.push('/login')
    } else if (registerMessage && typeof registerMessage !== 'object') {
      toast.error(registerMessage)
    }
  }

  handleNameChange = event => {
    this.setState({
      name: event.target.value
    })
  }

  handleEmailChange = event => {
    this.setState({
      email: event.target.value
    })
  }

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value
    })
  }

  handleConfirmPasswordChange = event => {
    this.setState({
      confirmPassword: event.target.value
    })
  }


  handleRegister = () => {
    const { email, password, confirmPassword, name } = this.state
    if(email && password && confirmPassword && name) {
      const payload = {
        email: email,
        password: password,
        password_confirm: confirmPassword,
        user_name: name
      }
      this.props.requestUserRegister(payload)
    } else {
      toast.error('Please fill all the field')
    }
  }

  render () {
    const { email, password, confirmPassword, name } = this.state
    return (
      <div className='background-light-grey'>
        <div className='login-header'>
          <Link to='/' className='text-decoration-none'>
            <div className='login-logo'>
              <label className='color-custom cursor-pointer'>
                InforMe
              </label>
            </div>
          </Link>
        </div>
        <div className='signup-container'>
          <div className='login-inner-container'>
            <div className='padding-top-m login-text'>
              Create Account
            </div>
            <div className='login-subtext'>
              create a new profile
            </div>
            <input type='text' name='name' placeholder='Username' className='login-input' value={name} onChange={this.handleNameChange}/>
            <input type='text' name='email' placeholder='Email' className='login-input margin-top-s' value={email} onChange={this.handleEmailChange}/>
            <input type='password' name='password' placeholder='Password' className='login-input margin-top-s' value={password} onChange={this.handlePasswordChange}/>
            <input type='password' name='confirm-password' placeholder='Confirm Password' className='login-input margin-top-s' value={confirmPassword} onChange={this.handleConfirmPasswordChange}/>
            <div className='margin-top-s margin-left-xxs' onClick={this.handleRegister}>
              <button className='signin-button cursor-pointer'>Sign up</button>
            </div>

          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.registerData
})

const mapDispatchToProps = dispatch => {
  return {
    requestUserRegister: (payload) => dispatch(UserActions.registerRequest(payload)),
    cleanRegister: () => dispatch(UserActions.cleanRegister())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
