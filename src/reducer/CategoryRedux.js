import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  categoryRequest: null,
  categorySuccess: ['data'],
  categoryProductRequest: ['payload'],
  categoryProductSuccess: ['data'],
  clearCategoryProduct: null
})

export const CategoryTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  category: null,
  categoryProduct: null
}

export const categoryRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const categorySuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    category: data,
    fetching: false
  }
}

export const categoryProductRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const categoryProductSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    categoryProduct: data,
    fetching: false
  }
}

export const clearCategoryProduct = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    categoryProduct: null
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CATEGORY_REQUEST]: categoryRequest,
  [Types.CATEGORY_SUCCESS]: categorySuccess,
  [Types.CATEGORY_PRODUCT_REQUEST]: categoryProductRequest,
  [Types.CATEGORY_PRODUCT_SUCCESS]: categoryProductSuccess,
  [Types.CLEAR_CATEGORY_PRODUCT]: clearCategoryProduct
})
