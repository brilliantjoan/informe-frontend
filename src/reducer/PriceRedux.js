import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  priceRequest: ['payload'],
  priceSuccess: ['data']
})

export const PriceTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  priceData: null
}

export const priceRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const priceSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    priceData: data,
    fetching: false
  }
}
export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRICE_REQUEST]: priceRequest,
  [Types.PRICE_SUCCESS]: priceSuccess
})
