import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  loginRequest: ['payload'],
  loginSuccess: ['data'],
  loginFailed: ['data'],
  registerRequest: ['payload'],
  registerSuccess: ['data'],
  updateProfileRequest: ['payload'],
  updateProfileSuccess: ['data'],
  logout: null,
  cleanRegister: null
})

export const UserTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  loginFetch: null,
  userData: null,
  isLogin: null,
  registerData: null,
  updateData: null
}

export const loginRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    loginFetch: true
  }
}

export const loginSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    userData: data,
    loginFetch: false,
    isLogin: true
  }
}

export const loginFailed = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    userData: 'failed',
    loginFetch: false,
    isLogin: false
  }
}

export const registerRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const registerSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    registerData: data,
    fetching: false
  }
}

export const updateProfileRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const updateProfileSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    updateData: data,
    fetching: false
  }
}

export const logout = (state = INITIAL_STATE) => {
  return {
    ...state,
    userData: null
  }
}

export const cleanRegister = (state = INITIAL_STATE) => {
  return {
    ...state,
    registerData: null,
    userData: null,
    isLogin: null,
    loginFetch: null
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGIN_FAILED]: loginFailed,
  [Types.REGISTER_REQUEST]: registerRequest,
  [Types.REGISTER_SUCCESS]: registerSuccess,
  [Types.UPDATE_PROFILE_REQUEST]: updateProfileRequest,
  [Types.UPDATE_PROFILE_SUCCESS]: updateProfileSuccess,
  [Types.LOGOUT]: logout,
  [Types.CLEAN_REGISTER]: cleanRegister
})
