import CreateStore from './CreateStore'
import sagas from '../sagas/sagas'
import { combineReducers } from 'redux'

import { reducer as UserRedux } from './UserRedux'
import { reducer as CategoryRedux } from './CategoryRedux'
import { reducer as ReviewRedux } from './ReviewRedux'
import { reducer as CommentRedux } from './CommentRedux'
import { reducer as ProductRedux } from './ProductRedux'
import { reducer as PriceRedux } from './PriceRedux'

const reducers = {
  user: UserRedux,
  category: CategoryRedux,
  review: ReviewRedux,
  comment: CommentRedux,
  product: ProductRedux,
  price: PriceRedux
}

export default CreateStore(combineReducers(reducers), sagas)
