import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  allProductRequest: ['payload'],
  allProductSuccess: ['data'],
  popularProductRequest: null,
  popularProductSuccess: ['data'],
  productPeopleChooseRequest: ['payload'],
  productPeopleChooseSuccess: ['data'],
  productRequest: ['payload'],
  productSuccess: ['data'],
  product2Request: ['payload'],
  product2Success: ['data'],
  product3Request: ['payload'],
  product3Success: ['data'],
  searchRequest: ['payload'],
  searchSuccess: ['data'],
  upgradeProductRequest: ['payload'],
  upgradeProductSuccess: ['data'],
  wishRequest: ['payload'],
  wishSuccess: ['data'],
  insertWishRequest: ['payload'],
  insertWishSuccess: ['data'],
  deleteWishRequest: ['payload'],
  deleteWishSuccess: ['data'],
  clearProduct: null
})

export const ProductTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  allProductData: null,
  popularProductData: null,
  productPeopleChooseData: null,
  productDetail: null,
  product2Detail: null,
  product3Detail: null,
  searchData: null,
  upgradeProduct: null,
  wishData: null,
  insertWish: null,
  deleteWish: null
}

export const allProductRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const allProductSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    allProductData: data,
    fetching: false
  }
}

export const popularProductRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const popularProductSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    popularProductData: data,
    fetching: false
  }
}

export const productPeopleChooseRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const productPeopleChooseSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    productPeopleChooseData: data,
    fetching: false
  }
}

export const productRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const productSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    productDetail: data[0],
    fetching: false
  }
}

export const product2Request = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const product2Success = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    product2Detail: data[0],
    fetching: false
  }
}

export const product3Request = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const product3Success = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    product3Detail: data[0],
    fetching: false
  }
}

export const searchRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const searchSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    searchData: data,
    fetching: false
  }
}

export const upgradeProductRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const upgradeProductSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    upgradeProduct: data,
    fetching: false
  }
}

export const wishRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const wishSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    wishData: data,
    fetching: false
  }
}

export const insertWishRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const insertWishSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    insertWish: data,
    fetching: false
  }
}

export const deleteWishRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const deleteWishSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    deleteWish: data,
    fetching: false
  }
}

export const clearProduct = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    fetching: false,
    productDetail: null,
    product2Detail: null,
    product3Detail: null
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ALL_PRODUCT_REQUEST]: allProductRequest,
  [Types.ALL_PRODUCT_SUCCESS]: allProductSuccess,
  [Types.POPULAR_PRODUCT_REQUEST]: popularProductRequest,
  [Types.POPULAR_PRODUCT_SUCCESS]: popularProductSuccess,
  [Types.PRODUCT_PEOPLE_CHOOSE_REQUEST]: productPeopleChooseRequest,
  [Types.PRODUCT_PEOPLE_CHOOSE_SUCCESS]: productPeopleChooseSuccess,
  [Types.PRODUCT_REQUEST]: productRequest,
  [Types.PRODUCT_SUCCESS]: productSuccess,
  [Types.PRODUCT2_REQUEST]: product2Request,
  [Types.PRODUCT2_SUCCESS]: product2Success,
  [Types.PRODUCT3_REQUEST]: product3Request,
  [Types.PRODUCT3_SUCCESS]: product3Success,
  [Types.SEARCH_REQUEST]: searchRequest,
  [Types.SEARCH_SUCCESS]: searchSuccess,
  [Types.UPGRADE_PRODUCT_REQUEST]: upgradeProductRequest,
  [Types.UPGRADE_PRODUCT_SUCCESS]: upgradeProductSuccess,
  [Types.WISH_REQUEST]: wishRequest,
  [Types.WISH_SUCCESS]: wishSuccess,
  [Types.INSERT_WISH_REQUEST]: insertWishRequest,
  [Types.INSERT_WISH_SUCCESS]: insertWishSuccess,
  [Types.DELETE_WISH_REQUEST]: deleteWishRequest,
  [Types.DELETE_WISH_SUCCESS]: deleteWishSuccess,
  [Types.CLEAR_PRODUCT]: clearProduct
})
