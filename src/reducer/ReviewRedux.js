import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  reviewRequest: ['payload'],
  reviewSuccess: ['data'],
  insertReviewRequest: ['payload'],
  insertReviewSuccess: ['data']
})

export const ReviewTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  review: null,
  insertResponse: null
}

export const reviewRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const reviewSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    review: data,
    fetching: false
  }
}

export const insertReviewRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const insertReviewSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    insertResponse: data,
    fetching: false
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REVIEW_REQUEST]: reviewRequest,
  [Types.REVIEW_SUCCESS]: reviewSuccess,
  [Types.INSERT_REVIEW_REQUEST]: insertReviewRequest,
  [Types.INSERT_REVIEW_SUCCESS]: insertReviewSuccess
})
