import { createReducer, createActions } from 'reduxsauce'

// Types and Action Creators
const { Types, Creators } = createActions({
  commentRequest: ['payload'],
  commentSuccess: ['data'],
  insertCommentRequest: ['payload'],
  insertCommentSuccess: ['data'],
  insertReplyRequest: ['payload'],
  insertReplySuccess: ['data'],
  deleteCommentRequest: ['payload'],
  deleteCommentSuccess: ['data']
})

export const CommentTypes = Types
export default Creators

// Initial State
const INITIAL_STATE = {
  fetching: false,
  comment: null,
  insertResponse: null,
  insertReplyResponse: null,
  deleteCommentResponse: null
}

export const commentRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const commentSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    comment: data,
    fetching: false
  }
}

export const insertCommentRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const insertCommentSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    insertResponse: data,
    fetching: false
  }
}

export const insertReplyRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const insertReplySuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    insertReplyResponse: data,
    fetching: false
  }
}

export const deleteCommentRequest = (state = INITIAL_STATE) => {
  return {
    ...state,
    fetching: true
  }
}

export const deleteCommentSuccess = (state = INITIAL_STATE, { data }) => {
  return {
    ...state,
    deleteCommentResponse: data,
    fetching: false
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.COMMENT_REQUEST]: commentRequest,
  [Types.COMMENT_SUCCESS]: commentSuccess,
  [Types.INSERT_COMMENT_REQUEST]: insertCommentRequest,
  [Types.INSERT_COMMENT_SUCCESS]: insertCommentSuccess,
  [Types.INSERT_REPLY_REQUEST]: insertReplyRequest,
  [Types.INSERT_REPLY_SUCCESS]: insertReplySuccess,
  [Types.DELETE_COMMENT_REQUEST]: deleteCommentRequest,
  [Types.DELETE_COMMENT_SUCCESS]: deleteCommentSuccess
})
