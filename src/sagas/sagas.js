import { call, put, takeLatest } from 'redux-saga/effects'

import UserActions, { UserTypes } from '../reducer/UserRedux'
import CategoryActions, { CategoryTypes } from '../reducer/CategoryRedux'
import ReviewActions, { ReviewTypes } from '../reducer/ReviewRedux'
import CommentActions, { CommentTypes } from '../reducer/CommentRedux'
import ProductActions, { ProductTypes } from '../reducer/ProductRedux'
import PriceActions, { PriceTypes } from '../reducer/PriceRedux'

import {
  fetchUser, fetchRegister, fetchAllCategory, fetchAllReview, fetchAllComment, insertReviewApi, insertCommentApi,
  insertReplyApi, deleteCommentApi, fetchProductDetail, fetchAllProduct, fetchPriceData, fetchCategoryProductData,
  fetchSearchData, fetchUpgradeData, fetchWishData, insertWishApi, deleteWishApi, fetchPopularProduct,
  fetchProductPeopleChoose, updateProfileApi
} from './getApi'

function * loginUser (action) {
  try {
    const data = yield call(fetchUser, action)
    if (data !== 'failed') {
      yield put(UserActions.loginSuccess(data))
    } else {
      yield put(UserActions.loginFailed(data))
    }
  } catch (e) {
    console.log(e)
  }
}

function * registerUser (action) {
  try {
    const data = yield call(fetchRegister, action)
    yield put(UserActions.registerSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getAllCategory () {
  try {
    const data = yield call(fetchAllCategory)
    yield put(CategoryActions.categorySuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getAllReview (action) {
  try {
    const data = yield call(fetchAllReview, action)
    yield put(ReviewActions.reviewSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getAllComment (action) {
  try {
    const data = yield call(fetchAllComment, action)
    yield put(CommentActions.commentSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * insertReview (action) {
  try {
    const data = yield call(insertReviewApi, action)
    yield put(ReviewActions.insertReviewSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * insertComment (action) {
  try {
    const data = yield call(insertCommentApi, action)
    yield put(CommentActions.insertCommentSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * insertReply (action) {
  try {
    const data = yield call(insertReplyApi, action)
    yield put(CommentActions.insertReplySuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * deleteComment (action) {
  try {
    const data = yield call(deleteCommentApi, action)
    yield put(CommentActions.deleteCommentSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getAllProduct (action) {
  try {
    const data = yield call(fetchAllProduct, action)
    yield put(ProductActions.allProductSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getPopularProduct (action) {
  try {
    const data = yield call(fetchPopularProduct, action)
    yield put(ProductActions.popularProductSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getProductPeopleChoose (action) {
  try {
    const data = yield call(fetchProductPeopleChoose, action)
    yield put(ProductActions.productPeopleChooseSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getProductDetail (action) {
  try {
    const data = yield call(fetchProductDetail, action)
    yield put(ProductActions.productSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getProduct2Detail (action) {
  try {
    const data = yield call(fetchProductDetail, action)
    yield put(ProductActions.product2Success(data))
  } catch (e) {
    console.log(e)
  }
}

function * getProduct3Detail (action) {
  try {
    const data = yield call(fetchProductDetail, action)
    yield put(ProductActions.product3Success(data))
  } catch (e) {
    console.log(e)
  }
}

function * getPriceData (action) {
  try {
    const data = yield call(fetchPriceData, action)
    yield put(PriceActions.priceSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getCategoryProductData (action) {
  try {
    const data = yield call(fetchCategoryProductData, action)
    yield put(CategoryActions.categoryProductSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getSearchData (action) {
  try {
    const data = yield call(fetchSearchData, action)
    yield put(ProductActions.searchSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getUpgradeData (action) {
  try {
    const data = yield call(fetchUpgradeData, action)
    yield put(ProductActions.upgradeProductSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * getWishData (action) {
  try {
    const data = yield call(fetchWishData, action)
    yield put(ProductActions.wishSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * insertWish (action) {
  try {
    const data = yield call(insertWishApi, action)
    yield put(ProductActions.insertWishSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * deleteWish (action) {
  try {
    const data = yield call(deleteWishApi, action)
    yield put(ProductActions.deleteWishSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

function * updateProfile (action) {
  try {
    const data = yield call(updateProfileApi, action)
    yield put(UserActions.updateProfileSuccess(data))
  } catch (e) {
    console.log(e)
  }
}

export default function * sagas () {
  yield takeLatest(UserTypes.LOGIN_REQUEST, loginUser)
  yield takeLatest(UserTypes.REGISTER_REQUEST, registerUser)
  yield takeLatest(CategoryTypes.CATEGORY_REQUEST, getAllCategory)
  yield takeLatest(ReviewTypes.REVIEW_REQUEST, getAllReview)
  yield takeLatest(CommentTypes.COMMENT_REQUEST, getAllComment)
  yield takeLatest(ReviewTypes.INSERT_REVIEW_REQUEST, insertReview)
  yield takeLatest(CommentTypes.INSERT_COMMENT_REQUEST, insertComment)
  yield takeLatest(CommentTypes.INSERT_REPLY_REQUEST, insertReply)
  yield takeLatest(CommentTypes.DELETE_COMMENT_REQUEST, deleteComment)
  yield takeLatest(ProductTypes.ALL_PRODUCT_REQUEST, getAllProduct)
  yield takeLatest(ProductTypes.POPULAR_PRODUCT_REQUEST, getPopularProduct)
  yield takeLatest(ProductTypes.PRODUCT_PEOPLE_CHOOSE_REQUEST, getProductPeopleChoose)
  yield takeLatest(ProductTypes.PRODUCT_REQUEST, getProductDetail)
  yield takeLatest(ProductTypes.PRODUCT2_REQUEST, getProduct2Detail)
  yield takeLatest(ProductTypes.PRODUCT3_REQUEST, getProduct3Detail)
  yield takeLatest(PriceTypes.PRICE_REQUEST, getPriceData)
  yield takeLatest(CategoryTypes.CATEGORY_PRODUCT_REQUEST, getCategoryProductData)
  yield takeLatest(ProductTypes.SEARCH_REQUEST, getSearchData)
  yield takeLatest(ProductTypes.UPGRADE_PRODUCT_REQUEST, getUpgradeData)
  yield takeLatest(ProductTypes.WISH_REQUEST, getWishData)
  yield takeLatest(ProductTypes.INSERT_WISH_REQUEST, insertWish)
  yield takeLatest(ProductTypes.DELETE_WISH_REQUEST, deleteWish)
  yield takeLatest(UserTypes.UPDATE_PROFILE_REQUEST, updateProfile)
}
