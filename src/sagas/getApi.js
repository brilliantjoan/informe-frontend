import config from '../config'
import axios from 'axios'

const baseApi = config.baseAPI

export const fetchUser = async action => {
  try {
    const api = `${baseApi}user/login`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, action.payload, header)
    if (response.data.data !== null) {
      const res = await response.data.data
      return res
    } else {
      const res = 'failed'
      return res
    }
  } catch (e) {
    console.log(e)
  }
}

export const fetchRegister = async action => {
  try {
    const api = `${baseApi}user/register`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, action.payload, header)
    const res = await response.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchAllCategory = async () => {
  try {
    const api = `${baseApi}product/listcategory`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchAllReview = async action => {
  try {
    const data = action.payload
    const api = `${baseApi}product/getAllReview/${data.productId}?limit=${data.limit}&offset=${data.offset}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchAllComment = async action => {
  try {
    const data = action.payload
    const api = `${baseApi}product/getAllComment/${data.productId}?limit=${data.limit}&offset=${data.offset}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const insertReviewApi = async action => {
  try {
    const data = {
      review_name: action.payload.name,
      review_comment: action.payload.review,
      star: action.payload.star
    }
    const api = `${baseApi}product/insert_review/${action.payload.productId}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const insertCommentApi = async action => {
  try {
    const data = {
      user_id: action.payload.userId,
      // name: action.payload.name,
      product_id: action.payload.productId,
      comment: action.payload.comment
    }
    const api = `${baseApi}product/insert_comment`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const insertReplyApi = async action => {
  try {
    const data = {
      user_id: action.payload.user_id,
      discussion_id: action.payload.discussion_id,
      reply_comment: action.payload.reply_comment
    }
    const api = `${baseApi}product/insert_reply_comment`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const deleteCommentApi = async action => {
  try {
    const data = {
      discussion_id: action.payload.discussion_id,
      reply_discussion_id: action.payload.reply_discussion_id
    }
    const api = `${baseApi}product/comment/delete`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchAllProduct = async action => {
  try {
    const api = `${baseApi}product/getallproduct`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchPopularProduct = async action => {
  try {
    const api = `${baseApi}product/getPopularProduct`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchProductPeopleChoose = async action => {
  try {
    const api = `${baseApi}product/getProductPeopleChoose?product_id=${action.payload}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchProductDetail = async action => {
  try {
    const data = action.payload
    const api = `${baseApi}product/getproductdetail/${data}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchPriceData = async action => {
  try {
    // const data = {
    //   product_id: action.payload.productId,
    //   name_product: action.payload.productName
    // }
    const api = `${baseApi}product/getPriceEcommerce?product_id=${action.payload.productId}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchCategoryProductData = async action => {
  try {
    const api = `${baseApi}product/getListDataByCategory?category_product=${action.payload}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchSearchData = async action => {
  try {
    const data = action.payload
    const api = `${baseApi}product/searchNavbar/${data}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchUpgradeData = async action => {
  try {
    const api = `${baseApi}product/upgrade?product_id=${action.payload.productId}&budget=${action.payload.budget}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const fetchWishData = async action => {
  try {
    const api = `${baseApi}user/get_wishlist/${action.payload}`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.get(api, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const insertWishApi = async action => {
  try {
    const data = {
      id: action.payload.userId,
      product_id: action.payload.productId
    }
    const api = `${baseApi}user/wishlist`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const deleteWishApi = async action => {
  try {
    const data = {
      id: action.payload.userId,
      product_id: action.payload.productId
    }
    const api = `${baseApi}user/delete_wishlist`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, data, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}

export const updateProfileApi = async action => {
  try {
    const api = `${baseApi}user/profile/edit`
    const header = {
      contentType: 'application/json'
    }
    const response = await axios.post(api, action.payload, header)
    const res = await response.data.data
    return res
  } catch (e) {
    console.log(e)
  }
}
